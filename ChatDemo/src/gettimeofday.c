#include "gettimeofday.h"

#if defined(_WIN32) || defined(_WIN64)

#include <time.h>

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	FILETIME ft;
	unsigned __int64 tmpres = 0;
	static int tzflag = 0;

	if (NULL != tv)
	{
		GetSystemTimeAsFileTime(&ft);

		tmpres |= ft.dwHighDateTime;
		tmpres <<= 32;
		tmpres |= ft.dwLowDateTime;

		tmpres /= 10;  /*convert into microseconds*/
					   /*converting file time to unix epoch*/
		tmpres -= DELTA_EPOCH_IN_MICROSECS;
		tv->tv_sec = (long)(tmpres / 1000000UL);
		tv->tv_usec = (long)(tmpres % 1000000UL);
	}

	if (NULL != tz)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}

		long timezone = 0;
		int daylight = 0;

		_get_timezone(&timezone);
		_get_daylight(&daylight);

		tz->tz_minuteswest = timezone / 60;
		tz->tz_dsttime = daylight;
	}

	return 0;
}

#endif

float timediffms(struct timeval t1, struct timeval t2)
{
	return (float)(t2.tv_sec - t1.tv_sec) * 1000.f + (float)(t2.tv_usec - t1.tv_usec) / 1000.f;
}
