#pragma once

#include "OpenSocketAPI.h"

#include "CommonTools/StaticArray.h"

struct CLIENT {
	OsaServerClient Client;
};

typedef char MESSAGE[256];

struct SERVER_CONTEXT {
	OsaServerContext Context = OSA_INVALID_CONTEXT;
	STATIC_ARRAY<CLIENT, 16> Clients;
	STATIC_ARRAY<MESSAGE, 256> Messages;
	//----------
	bool /*bSuccess*/ Open();
	void Close();
	void Update();
	void DrawUI();
};
