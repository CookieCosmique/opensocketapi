#pragma once

int IsHTTPGet(int Socket);
int ReadHandshakeQuery(int Socket, char* pDest, int DestSize);
int DoHandshake(int ClientSocket, const char* szRequest);
