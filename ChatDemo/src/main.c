#include "OpenSocketAPI.h"

#include <stdio.h>
#include <string.h>

#if defined(_WIN32)

#include <conio.h>

#else

#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#define strcpy_s(_Destination, _SizeInBytes, _Source) strcpy(_Destination, _Source)
#define sscanf_s(_Buffer, _Format, ...) sscanf(_Buffer, _Format, __VA_ARGS__)
#define sprintf_s(_Buffer, _BufferCount, _Format, ...) sprintf(_Buffer, _Format, __VA_ARGS__)

#define _kbhit kbhit
#define _getch getchar

int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}

#endif

#include "gettimeofday.h"

//-----------------------------------------------------------------------------

#define DEFAULT_LOCAL_IP_ADDRESS NULL
#define DEFAULT_REMOTE_IP_ADDRESS "5.51.81.153"
#define DEFAULT_PORT "200"

//-----------------------------------------------------------------------------

void OnError(const char* FunctionName, const char* FileName, int Line, const char* Condition, const char* ErrorStr) {
	printf("Error: %s:%d (%s) | %s\n\t%s\n", FileName, Line, FunctionName, Condition, ErrorStr);
}

//-----------------------------------------------------------------------------
//--------------------------------- SERVER ------------------------------------
//-----------------------------------------------------------------------------

typedef struct CLIENT {
	ServerClient Client;
	int Connected;
} CLIENT;

//-----------------------------------------------------------------------------

typedef struct SERVER_DATAS{
	CLIENT Clients[256];
	char History[256][OSA_MAX_PACKET_SIZE];
	int HistoryCount;
} SERVER_DATAS;

//-----------------------------------------------------------------------------

static void SendMessageToAllClients(ServerContext Context, void* ContextDatas, const char* Buffer, int BufferSize) {
	SERVER_DATAS* Datas = (SERVER_DATAS*)ContextDatas;
	sprintf_s(Datas->History[Datas->HistoryCount++], sizeof(Datas->History[0]), "%.*s", BufferSize, Buffer);
	for (int iClient = 0; iClient < 256; ++iClient) {
		if (Datas->Clients[iClient].Connected)
			SendPacketToClient(Context, Datas->Clients[iClient].Client, Buffer, BufferSize);
	}
}

//-----------------------------------------------------------------------------

static int OnServerClientConnect(ServerContext Context, void* ContextDatas, ServerClient Client, OUT void** ClientDatas) {
	// Add client to the list
	SERVER_DATAS* Datas = (SERVER_DATAS*)ContextDatas;
	for (int iClient = 0; iClient < 256; ++iClient) {
		if (!Datas->Clients[iClient].Connected) {
			Datas->Clients[iClient].Connected = 1;
			Datas->Clients[iClient].Client = Client;
			*ClientDatas = &Datas->Clients[iClient];
			break;
		}
	}

	// Send History to client
 	for (int iMessage = 0; iMessage < Datas->HistoryCount; ++iMessage) {
 		if (SendPacketToClient(Context, Client, Datas->History[iMessage], strlen(Datas->History[iMessage])))
 			break;
 	}

	// Send message to all clients
	char IpAddress[OSA_MAX_ADDRESS_LEN];
	char Port[OSA_MAX_PORT_LEN];
	GetClientAddressStr(Context, Client, IpAddress);
	GetClientPortStr(Context, Client, Port);
	char Message[OSA_MAX_PACKET_SIZE];
	const int MessageSize = sprintf_s(Message, sizeof(Message), "[%s:%s] Joined the server.", IpAddress, Port);
	printf("%s\n", Message);
	SendMessageToAllClients(Context, ContextDatas, Message, MessageSize);

	return 0;
}

//-----------------------------------------------------------------------------

static int OnServerClientDisconnect(ServerContext Context, void* ContextDatas, ServerClient Client, void* ClientDatas) {
	// Remove client from the list
	CLIENT* Datas = (CLIENT*)ClientDatas;
	Datas->Connected = 0;

	// Send message to all clients
	char IpAddress[OSA_MAX_ADDRESS_LEN];
	char Port[OSA_MAX_PORT_LEN];
	GetClientAddressStr(Context, Client, IpAddress);
	GetClientPortStr(Context, Client, Port);
	char Message[OSA_MAX_PACKET_SIZE];
	const int MessageSize = sprintf_s(Message, sizeof(Message), "[%s:%s] Left the server.", IpAddress, Port);
	printf("%s\n", Message);
	SendMessageToAllClients(Context, ContextDatas, Message, MessageSize);

	return 0;
}

//-----------------------------------------------------------------------------

static int OnServerPacketReceived(ServerContext Context, void* ContextDatas, ServerClient Client, void* ClientDatas, const char* Buffer, int BufferSize) {
	char IpAddress[OSA_MAX_ADDRESS_LEN];
	char Port[OSA_MAX_PORT_LEN];
	GetClientAddressStr(Context, Client, IpAddress);
	GetClientPortStr(Context, Client, Port);

	// Execute commands
	if (Buffer[0] == '/') {
		printf("[%s:%s] Run command: %.*s\n", IpAddress, Port, BufferSize, Buffer);
		if (strncmp(Buffer, "/ping", BufferSize) == 0) {
			return SendPacketToClient(Context, Client, Buffer, BufferSize);
		} else {
			char Message[OSA_MAX_PACKET_SIZE];
			const int MessageSize = sprintf_s(Message, sizeof(Message), "Unknown command: %.*s", BufferSize, Buffer);
			printf("%.*s\n", MessageSize, Message);
			return SendPacketToClient(Context, Client, Message, MessageSize);
		}
	} else {
		// Send message to all clients
		char Message[OSA_MAX_PACKET_SIZE];
		const int MessageSize = sprintf_s(Message, sizeof(Message), "[%s:%s]: %.*s", IpAddress, Port, BufferSize, Buffer);
		printf("%s\n", Message);
		SendMessageToAllClients(Context, ContextDatas, Message, MessageSize);
	}

	(void)ClientDatas;
	return 0;
}

//-----------------------------------------------------------------------------

static int RunServer(const char* IpAddress, const char* Port) {
	// Init Datas
	SERVER_DATAS Datas;
	memset(&Datas, 0, sizeof(Datas));

	// Init Context
	ServerContextParameters ContextParameters;
	memset(&ContextParameters, 0, sizeof(ContextParameters));
	if (IpAddress != NULL)
		strcpy_s(ContextParameters.Address, sizeof(ContextParameters.Address), IpAddress);
	sscanf_s(Port, "%d", &ContextParameters.Port);
	ContextParameters.ServerErrorCallback = OnError;
	ContextParameters.UserDatas = &Datas;
	ServerContext Context = InitServerContext(ContextParameters);
	if (Context == OSA_INVALID_CONTEXT)
		return 1;

	char ServerAddress[OSA_MAX_ADDRESS_LEN];
	char ServerPort[OSA_MAX_PORT_LEN];
	GetServerAddressStr(Context, ServerAddress);
	GetServerPortStr(Context, ServerPort);
	printf("Server launched on %s:%s\n", ServerAddress, ServerPort);

	// Set Callbacks
	SetServerConnectCallback(Context, OnServerClientConnect);
	SetServerDisconnectCallback(Context, OnServerClientDisconnect);
	SetServerPacketReceivedCallback(Context, OnServerPacketReceived);

	// Server Loop
	while (1) {
		if (_kbhit()) {
			_getch();
			break;
		}
		ServerPollEvents(Context);
	}

	// Shutdown Context
	if (ShutdownServerContext(Context))
		return 1;

	return 0;
}

//-----------------------------------------------------------------------------
//--------------------------------- CLIENT ------------------------------------
//-----------------------------------------------------------------------------

typedef struct CLIENT_DATAS {
	char Buffer[OSA_MAX_PACKET_SIZE];
	int BufferSize;
	int ClientConnected;
	struct timeval LastPingTime;
} CLIENT_DATAS;

//-----------------------------------------------------------------------------

static void PrintMessage(CLIENT_DATAS* Datas, const char* Message) {
	for (int i = 0; i < Datas->BufferSize; ++i)
		printf("\b \b");
	printf("\r");
	printf("%s",Message);
	printf("%.*s", Datas->BufferSize, Datas->Buffer);
}

//-----------------------------------------------------------------------------

static int OnClientPacketReceived(ClientContext Context, void* ContextDatas, const char* Buffer, int BufferSize) {
	CLIENT_DATAS* Datas = (CLIENT_DATAS*)ContextDatas;

	char Message[OSA_MAX_PACKET_SIZE];

	if (strncmp(Buffer, "/ping", BufferSize) == 0) {
		struct timeval CurrentTime;
		gettimeofday(&CurrentTime, NULL);
		float Ping = timediffms(Datas->LastPingTime, CurrentTime);
		sprintf_s(Message, sizeof(Message), "Ping: %f ms\n", Ping);
	} else {
		sprintf_s(Message, sizeof(Message), "%.*s\n", BufferSize, Buffer);
	}

	PrintMessage(Datas, Message);
	(void)Context;
	return 0;
}

//-----------------------------------------------------------------------------

static int OnClientDisconnect(ClientContext Context, void* ContextDatas) {
	printf("Client disconnected from server\n");
	CLIENT_DATAS* Datas = (CLIENT_DATAS*)ContextDatas;
	Datas->ClientConnected = 0;
	(void)Context;
	return 0;
}

//-----------------------------------------------------------------------------

static int SendClientPacketToServer(ClientContext Context, CLIENT_DATAS* Datas, const char* Buffer, int BufferSize) {
	if (BufferSize == 0)
		return 0;

	if (strncmp(Buffer, "/exit", BufferSize) == 0) {
		Datas->ClientConnected = 0;
		return 1;
	} else if (strncmp(Buffer, "/ping", BufferSize) == 0) {
		gettimeofday(&Datas->LastPingTime, NULL);
		return SendPacketToServer(Context, Buffer, BufferSize);
	} else {
		return SendPacketToServer(Context, Buffer, BufferSize);
	}
}

//-----------------------------------------------------------------------------

int RunClient(const char* IpAddress, const char* Port) {
	// Init Datas
	CLIENT_DATAS Datas;
	memset(&Datas, 0, sizeof(Datas));

	// Init Context
	ClientContextParameters ContextParameters;
	memset(&ContextParameters, 0, sizeof(ContextParameters));
	if (IpAddress == NULL || Port == NULL) {
		printf("Invalid arguments\n");
		return 1;
	}
	strcpy_s(ContextParameters.Address, sizeof(ContextParameters.Address), IpAddress);
	sscanf_s(Port, "%d", &ContextParameters.Port);
	ContextParameters.ClientErrorCallback = OnError;
	ContextParameters.UserDatas = &Datas;
	ClientContext Context = InitClientContext(ContextParameters);
	if (Context == OSA_INVALID_CONTEXT)
		return 1;

	char ServerAddress[OSA_MAX_ADDRESS_LEN];
	char ServerPort[OSA_MAX_PORT_LEN];
	GetClientServerAddressStr(Context, ServerAddress);
	GetClientServerPortStr(Context, ServerPort);
	printf("Client connected to %s:%s\n", ServerAddress, ServerPort);

	// Set Callbacks
	SetClientPacketReceivedCallback(Context, OnClientPacketReceived);
	SetClientDisconnectCallback(Context, OnClientDisconnect);

	Datas.ClientConnected = 1;

	// Client Loop
	while (Datas.ClientConnected) {
		if (_kbhit()) {
			const char c = (char)_getch();
			if (c == '\b') {
				if (Datas.BufferSize > 0) {
					--Datas.BufferSize;
					printf("\b \b");
				}
			} else if (c != '\r' && c != '\n') {
				printf("%c", c);
				Datas.Buffer[Datas.BufferSize++] = c;
			} else {
				for (int i = 0; i < Datas.BufferSize; ++i)
					printf("\b \b");
				printf("\r");
				if (SendClientPacketToServer(Context, &Datas, Datas.Buffer, Datas.BufferSize) == OsaErrorDisconnected) {
					break;
				}
				Datas.BufferSize = 0;
			}
		}
		if (ClientPollEvents(Context) == OsaErrorDisconnected)
			break;
	}

	// Shutdown
	if (ShutdownClientContext(Context))
		return 1;

	return 0;
}

//-----------------------------------------------------------------------------
//---------------------------------- MAIN -------------------------------------
//-----------------------------------------------------------------------------

int main(int ac, char** av) {
	int Server = 0;

	if (ac > 1 && strcmp(av[1], "-s") == 0) {
		Server = 1;
	}

	const char* IpAddress = ac > 1 + Server ? av[1 + Server] : Server ? DEFAULT_LOCAL_IP_ADDRESS : DEFAULT_REMOTE_IP_ADDRESS;
	const char* Port = ac > 2 + Server ? av[2 + Server] : DEFAULT_PORT;

	int Result;

	if (Server)
		Result = RunServer(IpAddress, Port);
	else
		Result = RunClient(IpAddress, Port);

	return Result;
}
