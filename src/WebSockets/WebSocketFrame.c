#include "WebSocketFrame.h"

#include <string.h>
#include <stdio.h>

int EncodeWebSocketFrame(unsigned char* msg, int msg_length, unsigned char* buffer, int buffer_size) {
	(void)buffer_size;

	int pos = 0;
	int size = msg_length;
	buffer[pos++] = (unsigned char)0x82; // binary frame

	if (size <= 125) {
		buffer[pos++] = (unsigned char)size;
	} else if (size <= 65535) {
		buffer[pos++] = 126; //16 bit length follows

		buffer[pos++] = (size >> 8) & 0xFF; // leftmost first
		buffer[pos++] = size & 0xFF;
	} else { // >2^16-1 (65535)
		buffer[pos++] = 127; //64 bit length follows

												 // write 8 bytes length (significant first)

												 // since msg_length is int it can be no longer than 4 bytes = 2^32-1
												 // padd zeroes for the first 4 bytes
		for (int i = 3; i >= 0; i--) {
			buffer[pos++] = 0;
		}
		// write the actual 32bit msg_length in the next 4 bytes
		for (int i = 3; i >= 0; i--) {
			buffer[pos++] = ((size >> 8 * i) & 0xFF);
		}
	}
	memcpy((void*)(buffer + pos), msg, size);
	return (size + pos);
}

int DecodeWebSocketFrame(unsigned char* in_buffer, unsigned int in_length, unsigned char* out_buffer, unsigned int out_size, unsigned int* out_length, int* out_close_connection) {
	if (in_length < 3)
		return 0;
	const unsigned char msg_opcode = in_buffer[0] & 0x0F;
	*out_close_connection = 0;
	if (msg_opcode == 0x0) printf("Continuation frame\n");
	if (msg_opcode == 0x1) printf("Text frame\n");
	if (msg_opcode == 0x2) printf("Binary frame\n");
	if (msg_opcode == 0x8) {
		printf("Connection close frame\n");
		*out_close_connection = 1;
	}
	if (msg_opcode == 0x9) printf("Ping frame\n");
	if (msg_opcode == 0xA) printf("Pong frame\n");

	//const unsigned char msg_fin = (in_buffer[0] >> 7) & 0x01;
	const unsigned char msg_masked = (in_buffer[1] >> 7) & 0x01;

	unsigned int payload_length = 0;
	unsigned int pos = 2;
	unsigned int length_field = in_buffer[1] & (~0x80);
	unsigned int mask = 0;

	if (length_field <= 125) {
		payload_length = length_field;
	} else if (length_field == 126) {
		payload_length = ((in_buffer[2] << 8) | (in_buffer[3]));
		pos += 2;
	} else if (length_field == 127) {
		payload_length = (
			((unsigned long long int)(in_buffer[2]) << 56) |
			((unsigned long long int)(in_buffer[3]) << 48) |
			((unsigned long long int)(in_buffer[4]) << 40) |
			((unsigned long long int)(in_buffer[5]) << 32) |
			((unsigned long long int)(in_buffer[6]) << 24) |
			((unsigned long long int)(in_buffer[7]) << 16) |
			((unsigned long long int)(in_buffer[8]) << 8) |
			((unsigned long long int)(in_buffer[9]))
			);
		pos += 8;
	}

	if (in_length < payload_length + pos)
		return 0;

	if (msg_masked) {
		mask = *((unsigned int*)(in_buffer + pos));
		pos += 4;
		unsigned char* c = in_buffer + pos;
		for (unsigned int i = 0; i < payload_length; i++)
			c[i] = c[i] ^ ((unsigned char*)(&mask))[i % 4];
	}

	if (payload_length > out_size)
		return 0;

	memcpy((void*)out_buffer, (void*)(in_buffer + pos), payload_length);
	out_buffer[payload_length] = 0;
	*out_length = payload_length + 1;

	return 1;
}
