#include "MainLoop.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
void MainLoop(MAIN_LOOP_CALLBACK LoopCallback, CLOSE_REQUEST_CALLBACK, void* pUserData) {
	emscripten_set_main_loop_arg(LoopCallback, pUserData, 0, 1);
}
#else
void MainLoop(MAIN_LOOP_CALLBACK LoopCallback, CLOSE_REQUEST_CALLBACK CloseCallback, void* pUserData) {
	while (!CloseCallback(pUserData))
		LoopCallback(pUserData);
}
#endif
