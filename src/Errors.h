#pragma once

#include <stdio.h>

#include "Platform.h"

//-----------------------------------------------------------------------------

typedef struct DummyContext {
	// Do not change members
	SOCKET _Socket;

	char _IpAddress[OSA_MAX_ADDRESS_LEN];
	char _Port[OSA_MAX_PORT_LEN];

	OsaErrorCallback _ErrorCallback;
	//-------------------------------------
} DummyContext;

//-----------------------------------------------------------------------------

#define SendWsaErrorMessage(__CONDITION__, __ERROR__)																							\
do {																																			\
	char ErrorStr[256];																															\
	GetErrorMessage(ErrorStr, sizeof(ErrorStr), __ERROR__);																						\
	char Message[256];																															\
	sprintf_s(Message, sizeof(Message), "Error %d: %s", ErrorValue, ErrorStr);																	\
	((DummyContext*)Context)->_ErrorCallback(__FUNCTION__, __FILE__, __LINE__, #__CONDITION__, Message);										\
} while (0)

//-----------------------------------------------------------------------------

#define WsaErrorCheckReturn(__CONDITION__, __RETURN__)																							\
do {																																			\
	if (__CONDITION__) {																														\
		const int ErrorValue = GetLastErrorValue();																								\
		if (ErrorValue != ERROR_WOULDBLOCK && ErrorValue != ERROR_ISCONN) {																		\
			if (((DummyContext*)Context)->_ErrorCallback != NULL) {																				\
				SendWsaErrorMessage(__CONDITION__, ErrorValue);																					\
			}																																	\
			return (__RETURN__);																												\
		}																																		\
	}																																			\
} while (0)

//-----------------------------------------------------------------------------

#define GetAddrInfoErrorCheckReturn(__GAIFUNCTION__, __RETURN__)																				\
do {																																			\
	const int ErrorValue = __GAIFUNCTION__;																										\
	if (ErrorValue != 0) {																														\
		if (((DummyContext*)Context)->_ErrorCallback != NULL) {																					\
			char Message[256];																													\
			sprintf_s(Message, sizeof(Message), "Error %d: %s", ErrorValue, gai_strerror(ErrorValue));											\
			((DummyContext*)Context)->_ErrorCallback(__FUNCTION__, __FILE__, __LINE__, #__GAIFUNCTION__, Message);								\
		}																																		\
		return (__RETURN__);																													\
	}																																			\
} while (0)

//-----------------------------------------------------------------------------

#define ErrorReturnValue(__CONDITION__, __ERROR__,  __RETURN_VALUE__)																			\
do {																																			\
	if (__CONDITION__) {																														\
		if ((uintptr_t)Context != OSA_INVALID_CONTEXT && (uintptr_t)Context != OSA_NULL_CONTEXT && ((DummyContext*)Context)->_ErrorCallback != NULL) {										\
			char Message[256];																													\
			sprintf_s(Message, sizeof(Message), "Error: %d", __ERROR__);																		\
			((DummyContext*)Context)->_ErrorCallback(__FUNCTION__, __FILE__, __LINE__, #__CONDITION__, Message);								\
		}																																		\
		return (__RETURN_VALUE__);																												\
	}																																			\
} while (0)

//-----------------------------------------------------------------------------

#define ReturnError(__CONDITION__, __ERROR__)																									\
do {																																			\
	if (__CONDITION__) {																														\
		if ((uintptr_t)Context != OSA_INVALID_CONTEXT && (uintptr_t)Context != OSA_NULL_CONTEXT && ((DummyContext*)Context)->_ErrorCallback != NULL) {										\
			char Message[256];																													\
			sprintf_s(Message, sizeof(Message), "Error: %d", __ERROR__);																		\
			((DummyContext*)Context)->_ErrorCallback(__FUNCTION__, __FILE__, __LINE__, #__CONDITION__, Message);								\
		}																																		\
		return (__ERROR__);																														\
	}																																			\
} while (0)

//-----------------------------------------------------------------------------
