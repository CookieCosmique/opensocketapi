# -----------------------------------------------------------------------------
# ------------------------ PROJECT SPECIFIC DEFINITIONS -----------------------
# -----------------------------------------------------------------------------

# Command args:
#   - COMPIL					(debug/release) (default: release)
#   - DBG						(gdb/lldb/...) (default: none)
#   - VERBOSE					(true/false) (default: false)

# Constants:
#   - PLATFORM					Platform name
#   - COMPIL					debug/release, is set with COMPIL option parameter (default: release)
#   - PRINT						Command to display debug messages

# Optional vars you can set:
#   - CPP_COMPILER				(default: clang++)
#   - C_COMPILER				(default: clang)
#   - CFLAGS					(default: -Wall -Wextra -Werror -fno-exceptions $(OPTIFLAGS))
#   - DEFINES					(no default value): List of C macro defined in all source files
#   - BUILD_DIR					(default: ./build)
#   - VERBOSE					(true/false) (default: false)

# Required vars:
#   For every modes:
#     - PROJECT_NAME
#     - TARGET_TYPE				(exe/lib)
#     - TARGET_NAME
#     - SRC_FILES				List of c/cpp files
#     - INCLUDE_DIRS
#   Only for exe mode:
#     - LINK_COMPILER			($(C_COMPILER)/$(CPP_COMPILER))
#     - LIBS_TO_BUILD			Libs that need a build (You need to create a rule for each lib in this list)
#     - ADDITIONAL_LIB_DIRS		List of directories to search for libraries
#     - LIBS_TO_LINK			Libs that will be linked

# -----------------------------------------------------------------------------

# *Project name
PROJECT_NAME	= SocketAPI

# *Target type (exe/lib)
TARGET_TYPE		= lib

# *Target name (without extension)
TARGET_NAME		= $(PROJECT_NAME)_$(PLATFORM)_$(COMPIL)

# -----------------------------------------------------------------------------

# *Source files
SRC_FILES		= src/ClientContext.c \
				  src/ServerContext.c

# *Include directories
INCLUDE_DIRS	= ./include

# -----------------------------------------------------------------------------

buildall:
	@$(MAKE) --no-print-directory $(MAKEOVERRIDES)
	@$(MAKE) -C ./ChatDemo/   --no-print-directory $(MAKEOVERRIDES)

.PHONY: buildall

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
