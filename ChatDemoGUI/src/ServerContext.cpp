#include "ServerContext.h"
//----
#include "CommonTools/Assert.h"
#include "CommonTools/Logger.h"
//----
#include <imgui.h>
//----
#include <stdio.h>
//-----------------------------------------------------------------------------
namespace {
	void SocketErrorCallback(const char* FunctionName, const char* FileName, int Line, const char* Condition, const char* ErrorStr) {
		AssertError(false);
		LOGGER::Log(LOGGER::LOG_ERROR, "%s (%s:%d) (%s - %s)", ErrorStr, FileName, Line, FunctionName, Condition);
	}
	//-----------------------------------------------------------------------------
	void SendMessageToAllClients(const SERVER_CONTEXT& m, const MESSAGE& Message) {
		for (uint32 iClient = 0; iClient < m.Clients.Count; ++iClient) {
			if (OsaServer_IsClientConnected(m.Context, m.Clients[iClient].Client))
				OsaServer_SendPacketToClient(m.Context, m.Clients[iClient].Client, (OsaByte*)Message, strlen(Message) + 1);
		}
	}
	//-----------------------------------------------------------------------------
	void NewClientMessage(SERVER_CONTEXT& m, OsaServerClient Client, const char* szMessage) {
		char IpAddress[OSA_MAX_ADDRESS_LEN];
		char Port[OSA_MAX_PORT_LEN];
		OsaServer_GetClientAddressStr(m.Context, Client, IpAddress);
		OsaServer_GetClientPortStr(m.Context, Client, Port);
		MESSAGE& NewMessage = m.Messages.Add();
		snprintf(NewMessage, sizeof(MESSAGE), "[%s:%s]: %s", IpAddress, Port, szMessage);
		SendMessageToAllClients(m, NewMessage);
	}
	//-----------------------------------------------------------------------------
	OsaBool ClientConnectionCallback(OsaServerContext /*Context*/, void* ContextDatas, OsaServerClient Client, OUT void** /*ClientDatas*/) {
		SERVER_CONTEXT& m = *(SERVER_CONTEXT*)ContextDatas;
		m.Clients.Add().Client = Client;
		NewClientMessage(m, Client, "Joined the chat.");
		return true;
	}
	//-----------------------------------------------------------------------------
	void RemoveClientFromList(SERVER_CONTEXT& m, OsaServerClient Client) {
		for (uint32 iClient = 0; iClient < m.Clients.Count; ++iClient) {
			if (m.Clients[iClient].Client == Client) {
				m.Clients.RemoveAt(iClient);
				return;
			}
		}
		AssertErrorAndLog(false, "Client not found.");
	}
	//-----------------------------------------------------------------------------
	void ClientDisconnectionCallback(OsaServerContext /*Context*/, void* ContextDatas, OsaServerClient Client, void* /*ClientDatas*/) {
		SERVER_CONTEXT& m = *(SERVER_CONTEXT*)ContextDatas;
		RemoveClientFromList(m, Client);
		NewClientMessage(m, Client, "Left the chat.");
	}
	//-----------------------------------------------------------------------------
	void PacketReceivedCallback(OsaServerContext /*Context*/, void* ContextDatas, OsaServerClient Client, void* /*ClientDatas*/, const OsaByte* Packet, unsigned int /*PacketSize*/) {
		SERVER_CONTEXT& m = *(SERVER_CONTEXT*)ContextDatas;
		NewClientMessage(m, Client, (const char*)Packet);
	}
	//-----------------------------------------------------------------------------
	OsaServerLaunchParameters FillContextParameters(SERVER_CONTEXT& Context) {
		OsaServerLaunchParameters ContextParameters = {};
		ContextParameters.Port = 200;
		ContextParameters.UserDatas = &Context;
		ContextParameters.ServerErrorCallback = SocketErrorCallback;
		return ContextParameters;
	}
	//-----------------------------------------------------------------------------
	void RegisterServerCallbacks(OsaServerContext Context) {
		OsaServer_SetClientConnectCallback(Context, ClientConnectionCallback);
		OsaServer_SetClientDisconnectCallback(Context, ClientDisconnectionCallback);
		OsaServer_SetPacketReceivedCallback(Context, PacketReceivedCallback);
	}
	//-----------------------------------------------------------------------------
	void LogServerAddress(OsaServerContext Context) {
		if (!LOGGER::IsEnabled())
			return;
		char ServerAddress[OSA_MAX_ADDRESS_LEN];
		char ServerPort[OSA_MAX_PORT_LEN];
		OsaServer_GetAddressStr(Context, ServerAddress);
		OsaServer_GetPortStr(Context, ServerPort);
		LOGGER::Log(LOGGER::LOG_INFO, "Server launched on %s:%s", ServerAddress, ServerPort);
	}
} //-- namespace
//-----------------------------------------------------------------------------
bool /*bSuccess*/ SERVER_CONTEXT::Open() {
	Clients.Clear();
	Messages.Clear();
	const OsaServerLaunchParameters ContextParameters = FillContextParameters(*this);
	Context = OsaServer_Launch(&ContextParameters);
	AssertErrorLogAndReturnValue(Context != OSA_INVALID_CONTEXT, false, "Failed to init server context.");
	RegisterServerCallbacks(Context);
	LogServerAddress(Context);
	return true;
}
//-----------------------------------------------------------------------------
void SERVER_CONTEXT::Close() {
	AssertErrorLogAndReturn(Context != OSA_INVALID_CONTEXT, "Cannot close server: Invalid context.");
	OsaServer_Shutdown(Context);
	Clients.Clear();
	Messages.Clear();
}
//-----------------------------------------------------------------------------
void SERVER_CONTEXT::Update() {
	OsaServer_PollEvents(Context);
}
//-----------------------------------------------------------------------------
void SERVER_CONTEXT::DrawUI() {
	SERVER_CONTEXT& m = *this;

	char ServerAddress[OSA_MAX_ADDRESS_LEN];
	char ServerPort[OSA_MAX_PORT_LEN];
	OsaServer_GetAddressStr(Context, ServerAddress);
	OsaServer_GetPortStr(Context, ServerPort);
	ImGui::Text("Server launched on %s:%s", ServerAddress, ServerPort);

	const int bCanReceive = OsaServer_CanAcceptConnections(Context);
	ImGui::Text("Status: %s", bCanReceive ? "ON" : "OFF");

	ImGui::Text("Clients: %d", Clients.Count);

	ImGui::PushStyleColor(ImGuiCol_ChildBg, 0xFF000000);

	if (ImGui::BeginChild("ChatMessages", ImVec2(ImGui::GetWindowWidth(), ImGui::GetContentRegionAvail().y))) {
		float CursorPosY = ImGui::GetWindowHeight() - ImGui::GetFontSize() - ImGui::GetStyle().ItemSpacing.y - ImGui::GetStyle().FramePadding.y * 2.f;
		ImGui::SetCursorPosY(CursorPosY);
		static MESSAGE InputMessage = {};
		static bool bFocusRequested = true;
		if (bFocusRequested) {
			ImGui::SetKeyboardFocusHere();
			bFocusRequested = false;
		}
		if (ImGui::InputText("", OUT InputMessage, sizeof(InputMessage), ImGuiInputTextFlags_EnterReturnsTrue)) {
			MESSAGE& NewMessage = m.Messages.Add();
			snprintf(NewMessage, sizeof(MESSAGE), "Server: %s", InputMessage);
			SendMessageToAllClients(m, NewMessage);
			InputMessage[0] = '\0';
			bFocusRequested = true;
		}
		for (int iMessage = (int)m.Messages.Count - 1; iMessage >= 0; --iMessage) {
			const MESSAGE& Message = m.Messages[iMessage];
			CursorPosY -= ImGui::GetFontSize() + ImGui::GetStyle().ItemSpacing.y;
			ImGui::SetCursorPosY(CursorPosY);
			ImGui::Text("%s", Message);
		}
	}
	ImGui::EndChild();

	ImGui::PopStyleColor();
}
//-----------------------------------------------------------------------------
