#pragma once

namespace LOGGER {
	enum LOG_LEVEL {
		LOG_INFO = 0,
		LOG_WARNING,
		LOG_ERROR,
		LOG_FATAL,
		LOG_DEBUG,
	};
	void Enable();
	void Disable();
	bool IsEnabled();
	void Log(const LOG_LEVEL Level, char const* const szFormat, ...);
} //-- namespace LOGGER
