#pragma once

#include "CommonTools/CommonDefines.h"
#include "Platform/Break.h"

#if DEBUG()
	#define Assert(__CONDITION__) do { if (!(__CONDITION__)) { Break(); } } while (0)
#else //-- DEBUG()
	#define Assert(__CONDITION__) do { } while (0)
#endif //-- DEBUG()

#define AssertWarning(__CONDITION__) Assert(__CONDITION__)
#define AssertError(__CONDITION__) Assert(__CONDITION__)
#define AssertFatal(__CONDITION__) Assert(__CONDITION__)
#define ToDo() Assert(false)

#define AssertWarningAndLog(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_WARNING, __VA_ARGS__); } } while (0)
#define AssertErrorAndLog(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_ERROR, __VA_ARGS__); } } while (0)
#define AssertFatalAndLog(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_FATAL, __VA_ARGS__); } } while (0)

#define AssertWarningLogAndReturn(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_WARNING, __VA_ARGS__); return; } } while (0)
#define AssertErrorLogAndReturn(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_ERROR, __VA_ARGS__); return; } } while (0)
#define AssertFatalLogAndReturn(__CONDITION__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_FATAL, __VA_ARGS__); return; } } while (0)

#define AssertWarningLogAndReturnValue(__CONDITION__, __VALUE__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_WARNING, __VA_ARGS__); return(__VALUE__); } } while (0)
#define AssertErrorLogAndReturnValue(__CONDITION__, __VALUE__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_ERROR, __VA_ARGS__); return(__VALUE__); } } while (0)
#define AssertFatalLogAndReturnValue(__CONDITION__, __VALUE__, ...) do { if (!(__CONDITION__)) { Break(); LOGGER::Log(LOGGER::LOG_FATAL, __VA_ARGS__); return(__VALUE__); } } while (0)
