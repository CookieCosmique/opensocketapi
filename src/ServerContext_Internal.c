#include "ServerContext_Internal.h"
#include "Errors.h"
#include "PacketHeader.h"
#include "Platform.h"

#include "WebSockets/WebSocketHandshake.h"
#include "WebSockets/WebSocketFrame.h"

#include <stdlib.h>

//-----------------------------------------------------------------------------
//------------------------- Context init/shutdown -----------------------------
//-----------------------------------------------------------------------------

static OsaServerContext FreeServerContext(OsaServerContext_t* Context) {
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaServerContext FreeServerContext_Wsa(OsaServerContext_t* Context) {
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaServerContext FreeServerContext_Wsa_addrinfo(OsaServerContext_t* Context, struct addrinfo* Address) {
	freeaddrinfo(Address);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaServerContext FreeServerContext_Wsa_addrinfo_socket(OsaServerContext_t* Context, struct addrinfo* Address, SOCKET Socket) {
	closesocket(Socket);
	freeaddrinfo(Address);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaServerContext FreeServerContext_Wsa_socket(OsaServerContext_t* Context, SOCKET Socket) {
	closesocket(Socket);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

//-----------------------------------------------------------------------------

OsaServerContext OsaServer_Launch_Internal(const OsaServerLaunchParameters* ServerLaunchParameters) {
	if (ServerLaunchParameters == NULL)
		return OSA_INVALID_CONTEXT;

	// Create Context
	OsaServerContext_t* Context = (OsaServerContext_t*)malloc(sizeof(OsaServerContext_t));
	memset(Context, 0, sizeof(OsaServerContext_t));
	Context->_ErrorCallback = ServerLaunchParameters->ServerErrorCallback;

	WsaErrorCheckReturn(InitSocketAPI() != 0, FreeServerContext(Context));

	// Init addrinfo
	struct addrinfo* ResultAddrInfo = NULL;
	struct addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	char PortStr[OSA_MAX_PORT_LEN];
	sprintf_s(PortStr, sizeof(PortStr), "%d", ServerLaunchParameters->Port);
	GetAddrInfoErrorCheckReturn(getaddrinfo(ServerLaunchParameters->Address, PortStr, &hints, &ResultAddrInfo), FreeServerContext_Wsa(Context));

	inet_ntop(ResultAddrInfo->ai_family, &((struct sockaddr_in*)ResultAddrInfo->ai_addr)->sin_addr, Context->_IpAddress, sizeof(Context->_IpAddress));
	sprintf_s(Context->_Port, sizeof(Context->_Port), "%d", ntohs(((struct sockaddr_in*)ResultAddrInfo->ai_addr)->sin_port));

	// Create socket
	SOCKET Socket = socket(ResultAddrInfo->ai_family, ResultAddrInfo->ai_socktype, ResultAddrInfo->ai_protocol);
	WsaErrorCheckReturn(Socket == INVALID_SOCKET, FreeServerContext_Wsa_addrinfo(Context, ResultAddrInfo));

	// Set non-blocking mode
	WsaErrorCheckReturn(SetNonBlocking(Socket) == SOCKET_ERROR, FreeServerContext_Wsa_socket(Context, Socket));

	// Bind socket
	WsaErrorCheckReturn(bind(Socket, ResultAddrInfo->ai_addr, (int)ResultAddrInfo->ai_addrlen) == SOCKET_ERROR, FreeServerContext_Wsa_addrinfo_socket(Context, ResultAddrInfo, Socket));
	freeaddrinfo(ResultAddrInfo);

	// Set listen mode
	WsaErrorCheckReturn(listen(Socket, SOMAXCONN) == SOCKET_ERROR, FreeServerContext_Wsa_socket(Context, Socket));

	Context->_Socket = Socket;
	Context->_UserDatas = ServerLaunchParameters->UserDatas;

	return (OsaServerContext)Context;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_Shutdown_Internal(OsaServerContext_t* Context) {
	for (size_t iClient = 0; iClient < OSA_MAX_CLIENTS_COUNT; ++iClient) {
		OsaServerClient_t* const Client = &Context->Clients[iClient];
		if (Client->_Connected) {
			OsaServer_DisconnectClient_Internal(Context, Client);
		}
	}

	WsaErrorCheckReturn(closesocket(Context->_Socket) != 0, OsaErrorSocket);
	WsaErrorCheckReturn(ShutdownSocketAPI() != 0, OsaErrorSocket);

	memset(Context, 0, sizeof(OsaServerContext_t));
	free(Context);

	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------ Callbacks ------------------------------------
//-----------------------------------------------------------------------------

OsaError OsaServer_SetClientConnectCallback_Internal(OsaServerContext_t* Context, OsaServerClientConnectCallback Callback) {
	Context->_ServerClientConnectCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SetClientDisconnectCallback_Internal(OsaServerContext_t* Context, OsaServerClientDisconnectCallback Callback) {
	Context->_ServerClientDisconnectCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SetPacketReceivedCallback_Internal(OsaServerContext_t* Context, OsaServerPacketReceivedCallback Callback) {
	Context->_ServerPacketReceivedCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Polling -------------------------------------
//-----------------------------------------------------------------------------

static OsaBool AcceptClient(OsaServerContext_t* Context, OsaServerClient_t* Client) {
	struct sockaddr_in Name;
	socklen_t NameSize = sizeof(Name);
	memset(&Name, 0, NameSize);
	SOCKET ClientSocket = accept(Context->_Socket, (struct sockaddr*)&Name, &NameSize);
	WsaErrorCheckReturn(ClientSocket == INVALID_SOCKET, 0);
	if (ClientSocket == 0 || ClientSocket == INVALID_SOCKET)
		return OSA_FALSE;
	memset(Client, 0, sizeof(*Client));
	Client->_Socket = ClientSocket;
	Client->_Connected = OSA_TRUE;
	inet_ntop(Name.sin_family, &Name.sin_addr, Client->_IpAddress, sizeof(Client->_IpAddress));
	sprintf_s(Client->_Port, sizeof(Client->_Port), "%d", ntohs(Name.sin_port));
	Client->_Type = OsaClientTypeUnknown;
	return OSA_TRUE;
}

//-----------------------------------------------------------------------------

static void AcceptClients(OsaServerContext_t* Context) {
	for (size_t iClient = 0; iClient < OSA_MAX_CLIENTS_COUNT; ++iClient) {
		OsaServerClient_t* const Client = &Context->Clients[iClient];
		if (!Client->_Connected) {
			if (!AcceptClient(Context, Client))
				break;
		}
	}
}

//-----------------------------------------------------------------------------

static OsaBool DoWebSocketClientHandshake(OsaServerClient_t* Client) {
	if (Client->_Type != OsaClientTypeUnknown)
		return OSA_FALSE;
	if (IsHTTPGet(Client->_Socket)) {
		char Buffer[2048];
		if (ReadHandshakeQuery(Client->_Socket, OUT Buffer, sizeof(Buffer))) {
			DoHandshake(Client->_Socket, Buffer);
		}
		Client->_Type = OsaClientTypeWebSocket;
		return OSA_TRUE;
	}
	return OSA_FALSE;
}

//-----------------------------------------------------------------------------

static OsaBool IsSpecialPacket(SOCKET Socket, int Value) {
	int Packet;
	const int res = recv(Socket, (char*)&Packet, sizeof(Packet), MSG_PEEK);
	if (res == SOCKET_ERROR || res == 0)
		return OSA_FALSE;
	if (Packet != Value)
		return OSA_FALSE;
	recv(Socket, (char*)&Packet, sizeof(Packet), 0);
	return OSA_TRUE;
}

//-----------------------------------------------------------------------------

static OsaBool IsConnectionPacket(SOCKET Socket) {
	return IsSpecialPacket(Socket, CONNECTION_PACKET);
}

//-----------------------------------------------------------------------------

static OsaBool IsDisconnectionPacket(SOCKET Socket) {
	return IsSpecialPacket(Socket, DISCONNECTION_PACKET);
}


//-----------------------------------------------------------------------------

static OsaBool ReadConnectionPacket(OsaServerClient_t* Client) {
	if (Client->_HasSentConnectionPacket)
		return OSA_FALSE;
	if (IsConnectionPacket(Client->_Socket)) {
		if (Client->_Type == OsaClientTypeUnknown)
			Client->_Type = OsaClientTypeSocket;
		Client->_HasSentConnectionPacket = OSA_TRUE;
		return OSA_TRUE;
	}
	return OSA_FALSE;
}

//-----------------------------------------------------------------------------

static OsaBool /*PacketReceived*/ ReadClientWebSocketPacket(OsaServerContext_t* Context, OsaServerClient_t* Client, OUT OsaByte* Packet, OUT unsigned int* PacketSize) {
	OsaByte EncodedBuffer[2048];
	const int Result = recv(Client->_Socket, (char*)EncodedBuffer, sizeof(EncodedBuffer), 0);
	if (Result == 0 || (Result == SOCKET_ERROR && (GetLastErrorValue() == ERROR_CONNRESET || GetLastErrorValue() == ERROR_CONNABORTED))) {
		OsaServer_DisconnectClient_Internal(Context, Client);
		return OSA_FALSE;
	}
	WsaErrorCheckReturn(Result == SOCKET_ERROR, 0);
	if (Result == -1)
		return OSA_FALSE;

	// Decode Packet
	const unsigned int EncodedBufferSize = (unsigned int)Result;
	OsaByte DecodedBuffer[2048];
	unsigned int DecodedBufferLength;
	int CloseConnection = 0;
	if (DecodeWebSocketFrame(EncodedBuffer, EncodedBufferSize, DecodedBuffer, sizeof(DecodedBuffer), &DecodedBufferLength, OUT &CloseConnection) == 0)
		return OSA_FALSE;
	if (CloseConnection) {
		OsaServer_DisconnectClient_Internal(Context, Client);
		return OSA_FALSE;
	}
	if (DecodedBufferLength < sizeof(int))
		return OSA_FALSE;

	// Read Connection/Disconnection Packets
	const unsigned int PacketType = *(unsigned int*)DecodedBuffer;
	if (!Client->_HasSentConnectionPacket) {
		if (PacketType == CONNECTION_PACKET) {
			Client->_HasSentConnectionPacket = OSA_TRUE;
			if (Context->_ServerClientConnectCallback != NULL && !Context->_ServerClientConnectCallback((OsaServerContext)Context, Context->_UserDatas, (OsaServerClient)Client, &Client->_UserDatas)) {
				OsaServer_DisconnectClient_Internal(Context, Client);
			}
		}
		return OSA_FALSE;
	}

	if (PacketType == DISCONNECTION_PACKET) {
		OsaServer_DisconnectClient_Internal(Context, Client);
		return OSA_FALSE;
	}

	// Read Packet Header + Content
	if (DecodedBufferLength < sizeof(PacketHeader))
		return OSA_FALSE;
	const PacketHeader Header = *(PacketHeader*)DecodedBuffer;
	if (!IsHeaderValid(Header))
		return OSA_FALSE;
	Assert(Header.PacketSize > 0);
	ErrorReturnValue(Header.PacketSize == DecodedBufferLength - sizeof(PacketHeader), OsaErrorInvalidPacketHeader, 0);
	memcpy(Packet, DecodedBuffer + sizeof(PacketHeader), Header.PacketSize);
	*PacketSize = Header.PacketSize;
	return OSA_TRUE;
}

//-----------------------------------------------------------------------------

static OsaBool /*PacketReceived*/ ReadClientSocketPacket(OsaServerContext_t* Context, OsaServerClient_t* Client, OUT OsaByte* Packet, OUT unsigned int* PacketSize) {
	PacketHeader Header;

	if (!Client->_HasSentConnectionPacket) {
		if (ReadConnectionPacket(Client)) {
			if (Context->_ServerClientConnectCallback != NULL && !Context->_ServerClientConnectCallback((OsaServerContext)Context, Context->_UserDatas, (OsaServerClient)Client, &Client->_UserDatas)) {
				OsaServer_DisconnectClient_Internal(Context, Client);
			}
		}
		return OSA_FALSE;
	}

	if (IsDisconnectionPacket(Client->_Socket)) {
		OsaServer_DisconnectClient_Internal(Context, Client);
		return OSA_FALSE;
	}


	// Read packet header
	int Result = recv(Client->_Socket, (char*)&Header, sizeof(Header), 0);
	if (Result == 0 || (Result == SOCKET_ERROR && (GetLastErrorValue() == ERROR_CONNRESET || GetLastErrorValue() == ERROR_CONNABORTED))) {
		OsaServer_DisconnectClient_Internal(Context, Client);
		return OSA_FALSE;
	}
	WsaErrorCheckReturn(Result == SOCKET_ERROR, 0);
	if (Result == -1)
		return OSA_FALSE;
	//Assert(Result == sizeof(Header));
	if ((Result != sizeof(Header) || !IsHeaderValid(Header)))
		return OSA_FALSE;
	//ErrorReturnValue(Result != sizeof(Header) || !IsHeaderValid(Header), OsaErrorInvalidPacketHeader, -1);
	Assert(Header.PacketSize > 0);

	// Read packet
	if (Result > 0) {
		Result = recv(Client->_Socket, (char*)Packet, (int)Header.PacketSize, 0);
		if (Result == 0 || (Result == SOCKET_ERROR && GetLastErrorValue() == ERROR_CONNRESET)) {
			OsaServer_DisconnectClient_Internal(Context, Client);
			return 0;
		}
		WsaErrorCheckReturn(Result == SOCKET_ERROR, 0);
		Assert(Result == (int)Header.PacketSize);
		ErrorReturnValue(Result != (int)Header.PacketSize, OsaErrorInvalidPacketHeader, -1);
		*PacketSize = (unsigned int)Result;
		Client->_Type = OsaClientTypeSocket;
	}

	return Result > 0;
}

//-----------------------------------------------------------------------------

static OsaBool /*PacketReceived*/ ReadClientPacket(OsaServerContext_t* Context, OsaServerClient_t* Client, OUT OsaByte* Packet, OUT unsigned int* PacketSize) {
	if (DoWebSocketClientHandshake(Client))
		return OSA_FALSE;
	if (Client->_Type == OsaClientTypeWebSocket) {
		return ReadClientWebSocketPacket(Context, Client, OUT Packet, OUT PacketSize);
	} else {
		return ReadClientSocketPacket(Context, Client, OUT Packet, OUT PacketSize);
	}
}

//-----------------------------------------------------------------------------

static int InitReadSet(OsaServerContext_t* Context, fd_set* pfdr) {
	FD_ZERO(pfdr);
	int SetClientsCount = 0;
	for (size_t iClient = 0; iClient < OSA_MAX_CLIENTS_COUNT; ++iClient) {
		OsaServerClient_t* const Client = &Context->Clients[iClient];
		if (Client->_Connected) {
			FD_SET(Client->_Socket, pfdr);
			++SetClientsCount;
		}
	}
	if (SetClientsCount == 0)
		return OsaNoError;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	WsaErrorCheckReturn(select(OSA_MAX_CLIENTS_COUNT + 1, pfdr, NULL, NULL, &timeout) == SOCKET_ERROR, SOCKET_ERROR);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

static void ReadClientsPackets(OsaServerContext_t* Context) {
	fd_set fdr;
	if (InitReadSet(Context, &fdr) == SOCKET_ERROR)
		return;
	for (size_t iClient = 0; iClient < OSA_MAX_CLIENTS_COUNT; ++iClient) {
		OsaServerClient_t* const Client = &Context->Clients[iClient];
		if (Client->_Connected) {
			OsaBool PacketReceived = OSA_FALSE;
			do {
				if (!FD_ISSET(Client->_Socket, &fdr))
					continue;
				OsaByte Packet[OSA_MAX_PACKET_SIZE];
				unsigned int PacketSize;
				PacketReceived = ReadClientPacket(Context, Client, OUT Packet, OUT &PacketSize);
				if (PacketReceived && Context->_ServerPacketReceivedCallback != NULL) {
					Context->_ServerPacketReceivedCallback((OsaServerContext)Context, Context->_UserDatas, (OsaServerClient)Client, Client->_UserDatas, Packet, PacketSize);
				}
			} while (PacketReceived);
		}
	}
}

//-----------------------------------------------------------------------------

static int CanAcceptClients(OsaServerContext_t* Context) {
	fd_set fdr;
	FD_ZERO(&fdr);
	FD_SET(Context->_Socket, &fdr);
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	WsaErrorCheckReturn(select(Context->_Socket + 1, &fdr, NULL, NULL, &timeout) == SOCKET_ERROR, SOCKET_ERROR);
	return FD_ISSET(Context->_Socket, &fdr);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_PollEvents_Internal(OsaServerContext_t* Context) {
	const int CanAcceptClientsResult = CanAcceptClients(Context);
	ReturnError(CanAcceptClientsResult == SOCKET_ERROR, OsaErrorSocket);
	if (CanAcceptClientsResult)
		AcceptClients(Context);
	ReadClientsPackets(Context);
	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Actions -------------------------------------
//-----------------------------------------------------------------------------

static void BuildPacketBuffer(const OsaByte* Packet, unsigned int PacketSize, OUT OsaByte* PacketBuffer, OUT unsigned int* PacketBufferSize) {
	PacketHeader Header;
	InitHeader(OUT &Header);
	Header.PacketSize = PacketSize;
	memcpy_s(PacketBuffer, *PacketBufferSize, &Header, sizeof(Header));
	memcpy_s(PacketBuffer + sizeof(Header), *PacketBufferSize - sizeof(Header), Packet, Header.PacketSize);
	*PacketBufferSize = Header.PacketSize + sizeof(PacketHeader);
}

//-----------------------------------------------------------------------------

static int SendWebSocketPacket(OsaServerContext_t* Context, OsaServerClient_t* Client, const OsaByte* Packet, unsigned int PacketSize) {
	OsaByte PacketBuffer[OSA_MAX_PACKET_SIZE + sizeof(PacketHeader)];
	unsigned int PacketBufferSize = sizeof(PacketBuffer);
	BuildPacketBuffer(Packet, PacketSize, OUT PacketBuffer, OUT &PacketBufferSize);

	OsaByte EncodedBuffer[2048];
	const unsigned int EncodedBufferSize = EncodeWebSocketFrame(PacketBuffer, PacketBufferSize, EncodedBuffer, sizeof(EncodedBuffer));

	const int Result = send(Client->_Socket, (const char*)EncodedBuffer, (int)EncodedBufferSize, 0);
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OsaErrorSocket);
	if (Result == 0)
		OsaServer_DisconnectClient_Internal(Context, Client);
	Assert(Result == (int)EncodedBufferSize);

	return OsaNoError;
}

//-----------------------------------------------------------------------------

static int SendSocketPacket(OsaServerContext_t* Context, OsaServerClient_t* Client, const OsaByte* Packet, unsigned int PacketSize) {
	// Create packet buffer
	OsaByte PacketBuffer[OSA_MAX_PACKET_SIZE + sizeof(PacketHeader)];
	unsigned int PacketBufferSize = sizeof(PacketBuffer);
	BuildPacketBuffer(Packet, PacketSize, OUT PacketBuffer, OUT &PacketBufferSize);

	// Send packet buffer
	const int Result = send(Client->_Socket, (const char*)PacketBuffer, (int)PacketBufferSize, 0);
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OsaErrorSocket);
	if (Result == 0)
		OsaServer_DisconnectClient_Internal(Context, Client);
	Assert(Result == (int)PacketBufferSize);

	return OsaNoError;
}

//-----------------------------------------------------------------------------

static int CanWrite(OsaServerContext_t* Context, OsaServerClient_t* Client) {
	fd_set fdw;
	FD_ZERO(&fdw);
	FD_SET(Client->_Socket, &fdw);
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	WsaErrorCheckReturn(select(Client->_Socket + 1, NULL, &fdw, NULL, &timeout) == SOCKET_ERROR, SOCKET_ERROR);
	return FD_ISSET(Client->_Socket, &fdw);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SendPacketToClient_Internal(OsaServerContext_t* Context, OsaServerClient_t* Client, const OsaByte* Packet, unsigned int PacketSize) {
	const int CanWriteResult = CanWrite(Context, Client);
	ReturnError(CanWriteResult == SOCKET_ERROR, OsaErrorSocket);
	ReturnError(CanWriteResult == 0, OsaErrorClientNotConnected);
	ReturnError(PacketSize > OSA_MAX_PACKET_SIZE, OsaErrorPacketSize);
	ReturnError(Client->_Type == OsaClientTypeUnknown, OsaErrorClientNotConnected);

	if (Client->_Type == OsaClientTypeWebSocket) {
		return SendWebSocketPacket(Context, Client, Packet, PacketSize);
	} else {
		return SendSocketPacket(Context, Client, Packet, PacketSize);
	}
}

//-----------------------------------------------------------------------------

OsaError OsaServer_DisconnectClient_Internal(OsaServerContext_t* Context, OsaServerClient_t* Client) {
	ReturnError(!((OsaServerClient_t*)Client)->_Connected, OsaErrorClientNotConnected);
	if (Client->_HasSentConnectionPacket && Context->_ServerClientDisconnectCallback != NULL)
		Context->_ServerClientDisconnectCallback((OsaServerContext)Context, Context->_UserDatas, (OsaServerClient)Client, Client->_UserDatas);
	Client->_Connected = OSA_FALSE;
#ifndef __EMSCRIPTEN__
	WsaErrorCheckReturn(shutdown(Client->_Socket, SD_BOTH) == SOCKET_ERROR, OsaErrorSocket);
#endif
	WsaErrorCheckReturn(closesocket(Client->_Socket) == SOCKET_ERROR, OsaErrorSocket);
	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Reading -------------------------------------
//-----------------------------------------------------------------------------

OsaBool OsaServer_CanAcceptConnections_Internal(OsaServerContext_t* Context) {
	int Value;
	socklen_t Len = sizeof(Value);
	const int Result = getsockopt(Context->_Socket, SOL_SOCKET, SO_ACCEPTCONN, (char*)&Value, &Len);
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OSA_FALSE);
	return Value != 0;
}

//-----------------------------------------------------------------------------

OsaBool OsaServer_IsClientConnected_Internal(OsaServerClient_t* Client) {
	return Client->_Type != OsaClientTypeUnknown;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetAddressStr_Internal(OsaServerContext_t* Context, OUT char Address[OSA_MAX_ADDRESS_LEN]) {
	strcpy_s(Address, OSA_MAX_ADDRESS_LEN, Context->_IpAddress);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetPortStr_Internal(OsaServerContext_t* Context, OUT char Port[OSA_MAX_PORT_LEN]) {
	strcpy_s(Port, OSA_MAX_PORT_LEN, Context->_Port);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetClientAddressStr_Internal(OsaServerClient_t* Client, OUT char Address[OSA_MAX_ADDRESS_LEN]) {
	strcpy_s(Address, OSA_MAX_ADDRESS_LEN, Client->_IpAddress);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetClientPortStr_Internal(OsaServerClient_t* Client, OUT char Port[OSA_MAX_PORT_LEN]) {
	strcpy_s(Port, OSA_MAX_PORT_LEN, Client->_Port);
	return OsaNoError;
}

//-----------------------------------------------------------------------------
