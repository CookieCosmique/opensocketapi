#pragma once

#include "OpenSocketAPI.h"
#include "Platform.h"

typedef struct OsaClientContext_t {
	// Do not change members (see Errors.h)
	SOCKET _Socket;

	char _IpAddress[OSA_MAX_ADDRESS_LEN];
	char _Port[OSA_MAX_PORT_LEN];

	OsaErrorCallback _ErrorCallback;
	//-------------------------------------
	OsaClientDisconnectCallback _ClientConnectCallback;
	OsaClientPacketReceivedCallback _ClientPacketReceivedCallback;
	OsaClientDisconnectCallback _ClientDisconnectCallback;

	void* _UserDatas;
	OsaBool _HasSentConnectionPacket;
	OsaBool _Disconnected;
} OsaClientContext_t;

//-------- Context init/shutdown

OsaClientContext OsaClient_Connect_Internal(const OsaClientConnectParameters* ClientConnectParameters);
OsaError OsaClient_Disconnect_Internal(OsaClientContext_t* Context);

//-------- Callbacks

OsaError OsaClient_SetConnectCallback_Internal(OsaClientContext_t* Context, OsaClientConnectCallback Callback);
OsaError OsaClient_SetPacketReceivedCallback_Internal(OsaClientContext_t* Context, OsaClientPacketReceivedCallback Callback);
OsaError OsaClient_SetDisconnectCallback_Internal(OsaClientContext_t* Context, OsaClientDisconnectCallback Callback);

//-------- Polling

OsaError OsaClient_PollEvents_Internal(OsaClientContext_t* Context);

//-------- Actions

OsaError OsaClient_SendPacketToServer_Internal(OsaClientContext_t* Context, const OsaByte* Packet, unsigned int PacketSize);

//-------- Reading

OsaBool OsaClient_IsConnected_Internal(OsaClientContext_t* Context);

OsaError OsaClient_GetServerAddressStr_Internal(OsaClientContext_t* Context, OUT char Buffer[OSA_MAX_ADDRESS_LEN]);
OsaError OsaClient_GetServerPortStr_Internal(OsaClientContext_t* Context, OUT char Buffer[OSA_MAX_PORT_LEN]);
