#pragma once

typedef void (*MAIN_LOOP_CALLBACK)(void*);
typedef bool (*CLOSE_REQUEST_CALLBACK)(void*);
void MainLoop(MAIN_LOOP_CALLBACK LoopCallback, CLOSE_REQUEST_CALLBACK CloseCallback, void* pUserData);
