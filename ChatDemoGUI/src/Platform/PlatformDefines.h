#pragma once

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__linux__)
# define USE_GLEW() 1
#else
# define USE_GLEW() 0
#endif
