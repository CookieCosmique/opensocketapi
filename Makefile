# -----------------------------------------------------------------------------
# ------------------------- VARIABLE DEFINITIONS ------------------------------
# -----------------------------------------------------------------------------

# Remember to bump the version everytimes this file is edited
MAKEFILE_VERSION_MAJOR	= 1
MAKEFILE_VERSION_MINOR	= 0
MAKEFILE_VERSION_PATCH	= 3
MAKEFILE_VERSION		= $(MAKEFILE_VERSION_MAJOR).$(MAKEFILE_VERSION_MINOR).$(MAKEFILE_VERSION_PATCH)

# Platform name
PLATFORM				= $(shell uname -s)

# Arguments usage: COMPIL=debug / COMPIL=release (default: release)
ifeq ($(COMPIL), debug)
COMPIL					= debug
else
COMPIL					= release
endif

# Compilation options
CPP_COMPILER			= clang++
C_COMPILER				= clang
ifeq ($(COMPIL), debug)
OPTIFLAGS				= -g -D _DEBUG
else
OPTIFLAGS				= -O3
endif
CFLAGS					= -Wall -Wextra -Werror -fno-exceptions $(OPTIFLAGS)

# Cygwin needs -e option to print \n
ifneq (,$(findstring CYGWIN,$(PLATFORM)))
PRINT					= echo -e "[$(PROJECT_NAME)] "
else
PRINT					= echo "[$(PROJECT_NAME)] "
endif

# Verbose mode
ifeq ($(VERBOSE), true)
VERBOSE					= true
else
VERBOSE					= false
endif

# Generate build path
BUILD_DIR				= ./build

# Default rule to run if no rule specified
default: all

# ------------------------ PROJECT SPECIFIC DEFINITIONS -----------------------
include project.make
# -----------------------------------------------------------------------------

# Extract src names from path
SRC						= $(SRC_FILES)

# Add -I before every include dir for compil args
INCLUDE					= $(addprefix -I,$(INCLUDE_DIRS))

# Generate object dir path
OBJ_DIR					= $(BUILD_DIR)/objects/$(PROJECT_NAME)/$(PLATFORM)/$(COMPIL)

# Replace / and \ in obj path to store all .o in the same dir with a unique name
# Add a _ before names to avoid hidden files if starting with './' or '../'
OBJ_SLASH_SEP			= __SLASH__
OBJ_BSLASH_SEP			= __BSLASH__
OBJ						= $(addprefix _,$(subst \,$(OBJ_BSLASH_SEP),$(subst /,$(OBJ_SLASH_SEP),$(addsuffix .o,$(SRC)))))
OBJ_FILES				= $(addprefix $(OBJ_DIR)/,$(OBJ))

# For each .o, a .d file is created using -MMD option in clang command
DEP						= $(OBJ_FILES:%.o=%.d)

# Generate binary path
# build/bin/$(PROJECT_NAME)/$(TARGET_NAME)
BIN_DIR					= $(BUILD_DIR)/bin/$(PROJECT_NAME)
ifeq ($(TARGET_TYPE), exe)
BINARY					= $(BIN_DIR)/$(TARGET_NAME).exe
else ifeq ($(TARGET_TYPE), lib)
BINARY					= $(BIN_DIR)/lib$(TARGET_NAME).a
endif

# List of C macro defined in all source files
DEFINES_ARGS			= $(addprefix -D,$(DEFINES))

# Add -L before every library directory for link args
LIB_DIRS				= $(addprefix -L, $(ADDITIONAL_LIB_DIRS))

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# --------------------------- RULES DEFINITIONS -------------------------------
# -----------------------------------------------------------------------------

# Default rule
.ONESHELL:
all:
	@$(MAKE) --no-print-directory Hello $(MAKEOVERRIDES)
	@StartBuildTime=$$(date +%s%N)
	@$(MAKE) --no-print-directory $(OBJ_DIR) $(MAKEOVERRIDES)
	@$(MAKE) --no-print-directory $(BINARY) $(MAKEOVERRIDES)
	@EndBuildTime=$$(date +%s%N)
	@BuildTimeMs=$$(echo $$(((EndBuildTime-StartBuildTime)/1000000)))
	@$(PRINT) "Build Time: $$((BuildTimeMs/1000)).$$((BuildTimeMs%1000)) s"
	@$(PRINT) "----------------"

# Running options
run: all
	@$(PRINT) "Run: $(BINARY)"
	@$(DBG) ./$(BINARY)

# Print projet name and args
Hello:
	@$(PRINT) "--- Makefile - v$(MAKEFILE_VERSION) ---"
	@$(PRINT) "Project: $(PROJECT_NAME) ($(COMPIL))"
	@$(PRINT) "Type: $(TARGET_TYPE)"
	@$(PRINT) "Target: $(BINARY)"
	@$(PRINT) "Platform: $(PLATFORM)"
ifeq ($(VERBOSE), true)
	@$(PRINT) "Verbose mode ON"
else
	@$(PRINT) "Verbose mode OFF"
endif
	@$(PRINT) "--------"

# Link file
.ONESHELL:
ifeq ($(TARGET_TYPE), exe)
$(BINARY): $(LIBS_TO_BUILD) $(OBJ_FILES) $(BIN_DIR)
	@$(PRINT) "Creating $(BINARY)"
	@StartLinkTime=$$(date +%s%N)
ifeq ($(VERBOSE), true)
	@$(PRINT) "$(LINK_COMPILER) $(CFLAGS) -o $(BINARY) $(OBJ_FILES) $(LIB_DIRS) $(LIBS_TO_LINK)"
endif
	@$(LINK_COMPILER) $(CFLAGS) -o $(BINARY) $(OBJ_FILES) $(LIB_DIRS) $(LIBS_TO_LINK)
	@EndLinkTime=$$(date +%s%N)
	@LinkTimeMs=$$(echo $$(((EndLinkTime-StartLinkTime)/1000000)))
	@$(PRINT) "Link Time: $$((LinkTimeMs/1000)).$$((LinkTimeMs%1000)) s"
else ifeq ($(TARGET_TYPE), lib)
$(BINARY): $(OBJ_FILES) $(BIN_DIR)
	@$(PRINT) "Creating $(BINARY)"
	@StartLinkTime=$$(date +%s%N)
ifeq ($(VERBOSE), true)
	@$(PRINT) "ar rc $(BINARY) $(OBJ_FILES)"
endif
	@ar rc $(BINARY) $(OBJ_FILES)
ifeq ($(VERBOSE), true)
	@$(PRINT) "ranlib $(BINARY)"
endif
	@ranlib $(BINARY)
	@EndLinkTime=$$(date +%s%N)
	@LinkTimeMs=$$(echo $$(((EndLinkTime-StartLinkTime)/1000000)))
	@$(PRINT) "Link Time: $$((LinkTimeMs/1000)).$$((LinkTimeMs%1000)) s"
endif

# Use .d files to recompile .o files if headers are modified
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered
# by calling -include $(DEP).
# The -MMD flags additionaly creates a .d file with
# the same name as the .o file.
.SECONDEXPANSION:
$(OBJ_DIR)/_%.cpp.o: $$(subst $(OBJ_BSLASH_SEP),\,$$(subst $(OBJ_SLASH_SEP),/,%.cpp))
ifeq ($(VERBOSE), true)
	@$(PRINT) "    [CC] $<\n[$(PROJECT_NAME)]       -> $@"
	@$(PRINT) "$(CPP_COMPILER) $(CFLAGS) $(DEFINES_ARGS) -MMD -c $< -o $@ $(INCLUDE)"
else
	@$(PRINT) "    [CC] $<"
endif
	@$(CPP_COMPILER) $(CFLAGS) $(DEFINES_ARGS) -MMD -c $< -o $@ $(INCLUDE)

.SECONDEXPANSION:
$(OBJ_DIR)/_%.c.o: $$(subst $(OBJ_BSLASH_SEP),\,$$(subst $(OBJ_SLASH_SEP),/,%.c))
ifeq ($(VERBOSE), true)
	@$(PRINT) "    [CC] $<\n[$(PROJECT_NAME)]       -> $@"
	@$(PRINT) "$(C_COMPILER) $(CFLAGS) $(DEFINES_ARGS) -MMD -c $< -o $@ $(INCLUDE)"
else
	@$(PRINT) "    [CC] $<"
endif
	@$(C_COMPILER) $(CFLAGS) $(DEFINES_ARGS) -MMD -c $< -o $@ $(INCLUDE)

# Create obj directories
$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)

# Create bin directories
$(BIN_DIR):
	@mkdir -p $(BIN_DIR)

# Delete every .o and .d files
clean:
	@$(PRINT) "clean $(PROJECT_NAME)"
ifeq ($(VERBOSE), true)
	@$(PRINT) "rm -rf $(OBJ_FILES) $(DEP)"
endif
	@rm -rf $(OBJ_FILES) $(DEP)

# Delete binary file
fclean:
	@$(MAKE) --no-print-directory clean $(MAKEOVERRIDES)
	@$(PRINT) "flean $(PROJECT_NAME)"
ifeq ($(VERBOSE), true)
	@$(PRINT) "rm -rf $(BINARY)"
endif
	@rm -rf $(BINARY)

re:
	@$(MAKE) --no-print-directory fclean $(MAKEOVERRIDES)
	@$(MAKE) --no-print-directory all $(MAKEOVERRIDES)

.PHONY: default all clean fclean re Hello

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
