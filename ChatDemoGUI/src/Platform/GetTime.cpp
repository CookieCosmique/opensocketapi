#include "GetTime.h"

#if (defined(_WIN32) || defined(_WIN64)) && !defined(__CYGWIN__)

#include <windows.h>

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

#include <time.h>

uint64 GetCurrentTimeInMicroSeconds() {
	FILETIME FileTime;
	GetSystemTimeAsFileTime(&FileTime);
	uint64 TimeInMicroSeconds = 0;
	TimeInMicroSeconds |= FileTime.dwHighDateTime;
	TimeInMicroSeconds <<= 32;
	TimeInMicroSeconds |= FileTime.dwLowDateTime;
	TimeInMicroSeconds /= 10;
	TimeInMicroSeconds -= DELTA_EPOCH_IN_MICROSECS;
	return TimeInMicroSeconds;
}

#else
#include <sys/time.h>
uint64 GetCurrentTimeInMicroSeconds() {
	struct timeval tv;
	gettimeofday(&tv, 0);
	uint64 TimeInMicroSeconds = tv.tv_sec * 1000000UL + tv.tv_usec;
	return TimeInMicroSeconds;
}
#endif
