# -----------------------------------------------------------------------------
# ------------------------ PROJECT SPECIFIC DEFINITIONS -----------------------
# -----------------------------------------------------------------------------

# Command args:
#   - COMPIL					(debug/release) (default: release)
#   - DBG						(gdb/lldb/...) (default: none)
#   - VERBOSE					(true/false) (default: false)

# Constants:
#   - PLATFORM					Platform name
#   - COMPIL					debug/release, is set with COMPIL option parameter (default: release)
#   - PRINT						Command to display debug messages

# Optional vars you can set:
#   - CPP_COMPILER				(default: clang++)
#   - C_COMPILER				(default: clang)
#   - CFLAGS					(default: -Wall -Wextra -Werror -fno-exceptions $(OPTIFLAGS))
#   - DEFINES					(no default value): List of C macro defined in all source files
#   - BUILD_DIR					(default: ./build)
#   - VERBOSE					(true/false) (default: false)

# Required vars:
#   For every modes:
#     - PROJECT_NAME
#     - TARGET_TYPE				(exe/lib)
#     - TARGET_NAME
#     - SRC_FILES				List of c/cpp files
#     - INCLUDE_DIRS
#   Only for exe mode:
#     - LINK_COMPILER			($(C_COMPILER)/$(CPP_COMPILER))
#     - LIBS_TO_BUILD			Libs that need a build (You need to create a rule for each lib in this list)
#     - ADDITIONAL_LIB_DIRS		List of directories to search for libraries
#     - LIBS_TO_LINK			Libs that will be linked

# -----------------------------------------------------------------------------

# *Project name
PROJECT_NAME	= ChatDemo

# *Target type (exe/lib)
TARGET_TYPE		= exe

# *Target name (without extension)
TARGET_NAME		= $(PROJECT_NAME)_$(PLATFORM)_$(COMPIL)

# *Link compiler (Only for exe mode): can be $(C_COMPILER) or $(CPP_COMPILER)
LINK_COMPILER	= $(C_COMPILER)

BUILD_DIR		= ./../build

# -----------------------------------------------------------------------------

# *Source files
SRC_FILES		= src/main.c \
				  src/gettimeofday.c

# *Include directories
INCLUDE_DIRS	= ./../include/

# ---------------------------- ONLY FOR EXE MODE ------------------------------

# Libraries to link
SOCKETAPI		= $(BUILD_DIR)/bin/SocketAPI/libSocketAPI_$(PLATFORM)_$(COMPIL).a

# Libs that need a build (You need to create a rule for each lib in this list)
LIBS_TO_BUILD	= $(SOCKETAPI)

# Libs that will be linked
LIBS_TO_LINK	= $(LIBS_TO_BUILD)

# Rule to generate SocketAPI
$(SOCKETAPI):
	@$(MAKE) -C ../ --no-print-directory $(MAKEOVERRIDES)

# -----------------------------------------------------------------------------

# Running options
runclient: all
	@$(PRINT) "Run: $(BINARY)"
	@$(DBG) ./$(BINARY)

runserver: all
	@$(PRINT) "Run: $(BINARY)"
	@$(DBG) ./$(BINARY) -s

.PHONY: runclient runserver

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
