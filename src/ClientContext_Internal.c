#include "ClientContext_Internal.h"
#include "Errors.h"
#include "PacketHeader.h"
#include "Platform.h"

#include <stdlib.h>

//-----------------------------------------------------------------------------
//------------------------- Context init/shutdown -----------------------------
//-----------------------------------------------------------------------------

static OsaClientContext FreeClientContext(OsaClientContext_t* Context) {
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaClientContext FreeClientContext_Wsa(OsaClientContext_t* Context) {
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaClientContext FreeClientContext_Wsa_addrinfo(OsaClientContext_t* Context, struct addrinfo* Address) {
	freeaddrinfo(Address);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaClientContext FreeClientContext_Wsa_addrinfo_socket(OsaClientContext_t* Context, struct addrinfo* Address, SOCKET Socket) {
	closesocket(Socket);
	freeaddrinfo(Address);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

static OsaClientContext FreeClientContext_Wsa_socket(OsaClientContext_t* Context, SOCKET Socket) {
	closesocket(Socket);
	ShutdownSocketAPI();
	free(Context);
	return OSA_INVALID_CONTEXT;
}

//-----------------------------------------------------------------------------

OsaClientContext OsaClient_Connect_Internal(const OsaClientConnectParameters* ClientConnectParameters) {
	// Create Context
	OsaClientContext_t* Context = (OsaClientContext_t*)malloc(sizeof(OsaClientContext_t));
	memset(Context, 0, sizeof(OsaClientContext_t));
	Context->_ErrorCallback = ClientConnectParameters->ClientErrorCallback;

	WsaErrorCheckReturn(InitSocketAPI() != 0, FreeClientContext(Context));

	// Init addrinfo
	struct addrinfo* ResultAddrInfo = NULL;
	struct addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	char PortStr[OSA_MAX_PORT_LEN];
	sprintf_s(PortStr, sizeof(PortStr), "%d", ClientConnectParameters->Port);
	GetAddrInfoErrorCheckReturn(getaddrinfo(ClientConnectParameters->Address, PortStr, &hints, &ResultAddrInfo), FreeClientContext_Wsa(Context));

	inet_ntop(ResultAddrInfo->ai_family, &((struct sockaddr_in*)ResultAddrInfo->ai_addr)->sin_addr, Context->_IpAddress, sizeof(Context->_IpAddress));
	sprintf_s(Context->_Port, sizeof(Context->_Port), "%d", ntohs(((struct sockaddr_in*)ResultAddrInfo->ai_addr)->sin_port));

	// Create socket
	SOCKET Socket = socket(ResultAddrInfo->ai_family, ResultAddrInfo->ai_socktype, ResultAddrInfo->ai_protocol);
	WsaErrorCheckReturn(Socket == INVALID_SOCKET, FreeClientContext_Wsa_addrinfo(Context, ResultAddrInfo));

	// Set non-blocking mode
	WsaErrorCheckReturn(SetNonBlocking(Socket) == SOCKET_ERROR, FreeClientContext_Wsa_socket(Context, Socket));

	// Connect to server
	const int ConnectStatus = connect(Socket, ResultAddrInfo->ai_addr, (int)ResultAddrInfo->ai_addrlen);
	WsaErrorCheckReturn(ConnectStatus == SOCKET_ERROR && GetLastErrorValue() != ERROR_IN_PROGRESS, FreeClientContext_Wsa_addrinfo_socket(Context, ResultAddrInfo, Socket));
	freeaddrinfo(ResultAddrInfo);

	Context->_Socket = Socket;
	Context->_UserDatas = ClientConnectParameters->UserDatas;
	Context->_HasSentConnectionPacket = OSA_FALSE;
	Context->_Disconnected = OSA_FALSE;

	return (OsaClientContext)Context;
}

//-----------------------------------------------------------------------------

static OsaBool CanWrite(OsaClientContext_t* Context) {
	fd_set fdw;
	FD_ZERO(&fdw);
	FD_SET(Context->_Socket, &fdw);
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	WsaErrorCheckReturn(select(Context->_Socket + 1, NULL, &fdw, NULL, &timeout) == SOCKET_ERROR, SOCKET_ERROR);
	return FD_ISSET(Context->_Socket, &fdw);
}

//-----------------------------------------------------------------------------

static OsaError SendDisconnectionPacket(OsaClientContext_t* Context) {
	if (!OsaClient_IsConnected_Internal(Context))
		return OsaErrorClientNotConnected;
	const int ConnectionPacket = DISCONNECTION_PACKET;
	const int Result = send(Context->_Socket, (char*)&ConnectionPacket, sizeof(ConnectionPacket), 0);
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OsaErrorSocket);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_Disconnect_Internal(OsaClientContext_t* Context) {
	SendDisconnectionPacket(Context);

#ifndef __EMSCRIPTEN__
	if (OsaClient_IsConnected_Internal(Context)) {
		WsaErrorCheckReturn(shutdown(Context->_Socket, SD_BOTH) == SOCKET_ERROR, OsaErrorSocket);
	}
#endif
	WsaErrorCheckReturn(closesocket(Context->_Socket) == SOCKET_ERROR, OsaErrorSocket);

	WsaErrorCheckReturn(ShutdownSocketAPI() != 0, OsaErrorSocket);

	memset(Context, 0, sizeof(OsaClientContext_t));
	free(Context);

	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------ Callbacks ------------------------------------
//-----------------------------------------------------------------------------

OsaError OsaClient_SetConnectCallback_Internal(OsaClientContext_t* Context, OsaClientConnectCallback Callback) {
	Context->_ClientConnectCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SetPacketReceivedCallback_Internal(OsaClientContext_t* Context, OsaClientPacketReceivedCallback Callback) {
	Context->_ClientPacketReceivedCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SetDisconnectCallback_Internal(OsaClientContext_t* Context, OsaClientDisconnectCallback Callback) {
	Context->_ClientDisconnectCallback = Callback;
	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Polling -------------------------------------
//-----------------------------------------------------------------------------

static int CanRead(OsaClientContext_t* Context) {
	fd_set fdr;
	FD_ZERO(&fdr);
	FD_SET(Context->_Socket, &fdr);
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	WsaErrorCheckReturn(select(Context->_Socket + 1, &fdr, NULL, NULL, &timeout) == SOCKET_ERROR, SOCKET_ERROR);
	return FD_ISSET(Context->_Socket, &fdr);
}

//-----------------------------------------------------------------------------

static OsaError SendConnectionPacket(OsaClientContext_t* Context) {
	const int ConnectionPacket = CONNECTION_PACKET;
	const int Result = send(Context->_Socket, (char*)&ConnectionPacket, sizeof(ConnectionPacket), 0);
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OsaErrorSocket);
	Context->_HasSentConnectionPacket = OSA_TRUE;
	return OsaNoError;
}

//-----------------------------------------------------------------------------

static void OnClientDisconnected(OsaClientContext_t* Context) {
	if (Context->_ClientDisconnectCallback != NULL)
		Context->_ClientDisconnectCallback((OsaClientContext)Context, Context->_UserDatas);
	Context->_Disconnected = OSA_TRUE;
}

//-----------------------------------------------------------------------------

static OsaBool /*PacketReceived*/ ClientReadPacket(OsaClientContext_t* Context, OUT OsaByte* Packet, OUT unsigned int* PacketSize) {
	PacketHeader Header;

	// Read packet header
	int Result = recv(Context->_Socket, (char*)&Header, sizeof(Header), 0);
	if (Result == 0 || (Result == SOCKET_ERROR && GetLastErrorValue() == ERROR_CONNRESET)) {
		OnClientDisconnected(Context);
		return OSA_FALSE;
	}
	WsaErrorCheckReturn(Result == SOCKET_ERROR, 0);
	if (Result == -1)
		return OSA_FALSE;
	Assert(Result == sizeof(Header));
	Assert(Header.PacketSize > 0);
	ErrorReturnValue(Result != sizeof(Header) || !IsHeaderValid(Header), OsaErrorInvalidPacketHeader, -1);

	if (Result > 0) {
		// Read packet
		Result = recv(Context->_Socket, (char*)Packet, Header.PacketSize, 0);
		if (Result == 0 || (Result == SOCKET_ERROR && GetLastErrorValue() == ERROR_CONNRESET)) {
			OnClientDisconnected(Context);
			return OSA_FALSE;
		}
		WsaErrorCheckReturn(Result == SOCKET_ERROR, 0);
		Assert(Result == (int)Header.PacketSize);
		ErrorReturnValue(Result != (int)Header.PacketSize, OsaErrorInvalidPacketHeader, -1);
		*PacketSize = (int)Result;
	}

	return Result > 0;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_PollEvents_Internal(OsaClientContext_t* Context) {
	if (!Context->_HasSentConnectionPacket && CanWrite(Context)) {
		if (SendConnectionPacket(Context) == OsaNoError && Context->_ClientConnectCallback)
			Context->_ClientConnectCallback((OsaClientContext)Context, Context->_UserDatas);
	}

	const int CanReadResult = CanRead(Context);
	ReturnError(CanReadResult == SOCKET_ERROR, OsaErrorSocket);
	if (CanReadResult == 0)
		return OsaNoError;

	OsaBool PacketReceived = OSA_FALSE;

	do {
		OsaByte Packet[OSA_MAX_PACKET_SIZE];
		unsigned int PacketSize;
		PacketReceived = ClientReadPacket(Context, OUT Packet, OUT &PacketSize);
		if (PacketReceived && Context->_ClientPacketReceivedCallback != NULL)
			Context->_ClientPacketReceivedCallback((OsaClientContext)Context, Context->_UserDatas, Packet, PacketSize);
	} while (PacketReceived);

	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Actions -------------------------------------
//-----------------------------------------------------------------------------

OsaError OsaClient_SendPacketToServer_Internal(OsaClientContext_t* Context, const OsaByte* Packet, unsigned int PacketSize) {
	const int CanWriteResult = CanWrite(Context);
	ReturnError(CanWriteResult == SOCKET_ERROR, OsaErrorSocket);
	ReturnError(CanWriteResult == 0, OsaErrorClientNotConnected);
	ReturnError(PacketSize > OSA_MAX_PACKET_SIZE, OsaErrorPacketSize);

	// Create packet buffer
	PacketHeader Header;
	InitHeader(OUT &Header);
	Header.PacketSize = PacketSize;
	char PacketBuffer[OSA_MAX_PACKET_SIZE + sizeof(Header)];
	memcpy_s(PacketBuffer, sizeof(PacketBuffer), &Header, sizeof(Header));
	memcpy_s(PacketBuffer + sizeof(Header), sizeof(PacketBuffer) - sizeof(Header), Packet, Header.PacketSize);

	// Send packet buffer
	const int Result = send(Context->_Socket, (char*)PacketBuffer, (int)(Header.PacketSize + sizeof(Header)), 0);
	if (Result == 0 || (Result == SOCKET_ERROR && GetLastErrorValue() == ERROR_CONNRESET)) {
		OnClientDisconnected(Context);
		return OsaErrorDisconnected;
	}
	WsaErrorCheckReturn(Result == SOCKET_ERROR, OsaErrorSocket);
	Assert(Result == (int)(PacketSize + sizeof(Header)));

	return OsaNoError;
}

//-----------------------------------------------------------------------------
//------------------------------- Reading -------------------------------------
//-----------------------------------------------------------------------------

OsaBool OsaClient_IsConnected_Internal(OsaClientContext_t* Context) {
	return !Context->_Disconnected && Context->_HasSentConnectionPacket;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_GetServerAddressStr_Internal(OsaClientContext_t* Context, OUT char Buffer[OSA_MAX_ADDRESS_LEN]) {
	strcpy_s(Buffer, OSA_MAX_ADDRESS_LEN, Context->_IpAddress);
	return OsaNoError;
}

//-----------------------------------------------------------------------------

OsaError OsaClient_GetServerPortStr_Internal(OsaClientContext_t* Context, OUT char Buffer[OSA_MAX_PORT_LEN]) {
	strcpy_s(Buffer, OSA_MAX_PORT_LEN, Context->_Port);
	return OsaNoError;
}

//-----------------------------------------------------------------------------
