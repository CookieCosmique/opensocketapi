#include "Logger.h"

#include <stdio.h>
#include <stdarg.h>

#include "Platform/Types.h"
#include "CommonTools/Assert.h"

namespace LOGGER {
	//-----------------------------------------------------------------------------
	namespace {
		static bool bEnabled = false;
		constexpr const uint64 MAX_LOG_LENGTH = 1024;
		//----------
		const char* LogLevelToStr(const LOG_LEVEL Level) {
			switch (Level) {
				case LOG_INFO			: return "Info";
				case LOG_WARNING	: return "Warning";
				case LOG_ERROR		: return "Error";
				case LOG_FATAL		: return "Fatal";
				case LOG_DEBUG		: return "Debug";
				default						: AssertWarning(false); return "Unknown";
			}
		}
		//-----------------------------------------------------------------------------
		struct LOG_BUFFER {
		private:
			char szBuffer[MAX_LOG_LENGTH];
			int32 BufferPos = 0;
		public:
			void WriteLogLevelIntoBuffer(const LOG_LEVEL Level) {
				BufferPos += snprintf(OUT szBuffer + BufferPos, sizeof(szBuffer) - BufferPos, "%s: ", LogLevelToStr(Level));
			}
			//----------
			void WriteMessage(char const* const szFormat, va_list v) {
				BufferPos += vsnprintf(OUT szBuffer + BufferPos, sizeof(szBuffer) - BufferPos, szFormat, v);
			}
			//----------
			void AppendNewLineIfNeeded() {
				if (szBuffer[BufferPos - 1] != '\n') {
					szBuffer[BufferPos] = '\n';
					szBuffer[++BufferPos] = '\0';
				}
			}
			//----------
			void Print() const {
				printf("%s", szBuffer);
				fflush(stdout);
			}
		};
		//-----------------------------------------------------------------------------
		void LogV(const LOG_LEVEL Level, char const* const szFormat, va_list v) {
			if (!bEnabled)
				return;
			LOG_BUFFER Buffer;
			Buffer.WriteLogLevelIntoBuffer(Level);
			Buffer.WriteMessage(szFormat, v);
			Buffer.AppendNewLineIfNeeded();
			Buffer.Print();
		}
	} //-- namespace
	//-----------------------------------------------------------------------------
	void Enable() {
		bEnabled = true;
	}
	//-----------------------------------------------------------------------------
	void Disable() {
		bEnabled = false;
	}
	//-----------------------------------------------------------------------------
	bool IsEnabled() {
		return bEnabled;
	}
	//-----------------------------------------------------------------------------
	void Log(const LOG_LEVEL Level, char const* const szFormat, ...) {
		va_list v;
		va_start(v, szFormat);
		LogV(Level, szFormat, v);
		va_end(v);
	}
	//-----------------------------------------------------------------------------
} //-- namespace LOGGER
