#pragma once
//----
#include "Platform/Types.h"
//----
#include "CommonTools/Assert.h"
//-----------------------------------------------------------------------------
template <typename T_TYPE, uint32 T_SIZE>
struct STATIC_ARRAY {
	T_TYPE Data[T_SIZE];
	uint32 Count;
	//----
	STATIC_ARRAY(): Count(0) {}
	//----
	const T_TYPE& operator[](const uint32 Index) const {
		AssertFatal(Index < Count);
		return Data[Index];
	}
	//----
	T_TYPE& operator[](const uint32 Index) {
		AssertFatal(Index < Count);
		return Data[Index];
	}
	//----
	T_TYPE& Add() {
		AssertFatal(Count < T_SIZE);
		return Data[Count++];
	}
	//----
	void RemoveAt(const uint32 Index) {
		AssertError(Index < Count);
		if (Index != Count - 1)
			Data[Index] = Data[Count - 1];
		--Count;
	}
	//----
	void Clear() {
		Count = 0;
	}
};
//-----------------------------------------------------------------------------
