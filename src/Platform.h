#pragma once

#define DONOTHING (void)0

#if defined(_WIN32)
	#pragma comment(lib, "ws2_32.lib")

	#include <winsock2.h>
	#include <ws2tcpip.h>

	inline int InitSocketAPI() {
		WSADATA WsaData;
		return WSAStartup(MAKEWORD(2, 2), &WsaData);
	}

	inline int ShutdownSocketAPI() { return WSACleanup(); }
	inline int GetLastErrorValue() { return WSAGetLastError(); }
	inline void GetErrorMessage(OUT char* Buffer, int BufferSize, int Error) { FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, Error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), Buffer, BufferSize, NULL); }
	inline int SetNonBlocking(SOCKET Socket) { unsigned long b; return ioctlsocket(Socket, FIONBIO, &b); }

	#define ERROR_WOULDBLOCK WSAEWOULDBLOCK
	#define ERROR_IN_PROGRESS WSAEINPROGRESS
	#define ERROR_ISCONN WSAEISCONN
	#define ERROR_CONNRESET WSAECONNRESET
	#define ERROR_CONNABORTED WSAECONNABORTED

	#define Assert(__CONDITION__) do { if (!(__CONDITION__) && IsDebuggerPresent()) { DebugBreak(); } } while(0)
#elif defined(__linux__) || defined(__CYGWIN__) || defined(__EMSCRIPTEN__)
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <sys/ioctl.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <fcntl.h>

	#include <string.h>
	#include <unistd.h>
	#include <errno.h>

	#define strcpy_s(_Destination, _SizeInBytes, _Source) strcpy (_Destination, _Source)
	#define sprintf_s(_Buffer, _BufferCount, _Format, ...) sprintf(_Buffer, _Format, __VA_ARGS__)
	#define memcpy_s(_Destination, _DestinationSize, _Source, _SourceSize) memcpy(_Destination, _Source, _SourceSize)

	typedef int SOCKET;
#ifdef __EMSCRIPTEN__
	typedef socklen_t socklen;
#else
	typedef __socklen_t socklen;
#endif

	static inline int InitSocketAPI() { return 0; }
	static inline int ShutdownSocketAPI() { return 0; }
	static inline int closesocket(SOCKET s) { return close(s); }
	static inline int ioctlsocket(SOCKET s, long cmd, unsigned long* argp) { return ioctl(s, cmd, argp); }

	static inline int GetLastErrorValue() { return errno; }
	static inline void GetErrorMessage(OUT char* Buffer, int BufferSize, int Error) { strcpy(Buffer, strerror(Error)); (void)BufferSize; }
	static inline int SetNonBlocking(SOCKET Socket) { return fcntl(Socket, F_SETFL, O_NONBLOCK); }

	#define ERROR_WOULDBLOCK EWOULDBLOCK
	#define ERROR_IN_PROGRESS EINPROGRESS
	#define ERROR_ISCONN EISCONN
	#define ERROR_CONNRESET ECONNRESET
	#define ERROR_CONNABORTED ECONNABORTED

	#define SD_BOTH SHUT_RDWR

	#define Assert(__CONDITION__) DONOTHING
#else
	#error Unsupported platform
#endif
