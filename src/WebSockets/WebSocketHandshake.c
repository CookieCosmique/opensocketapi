#include "WebSocketHandshake.h"

#ifdef _WIN32
#include <winsock2.h>
#include <WS2tcpip.h>
typedef int socklen_t;
#define close closesocket
#pragma comment(lib, "Ws2_32.lib")
#else
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#endif

#include <string.h>

#include "sha1.h"
#include "base64.h"

//-----------------------------------------------------------------------------

int IsHTTPGet(int sockfd) {
	char Get[3];
	const int res = recv(sockfd, Get, sizeof(Get), MSG_PEEK);
	if (res == -1 || res == 0)
		return 0;
	return Get[0] == 'G' && Get[1] == 'E' && Get[2] == 'T';
}

//-----------------------------------------------------------------------------

int ReadHandshakeQuery(int Socket, char* pDest, int DestSize) {
	const int res = recv(Socket, pDest, DestSize, MSG_PEEK);
	if (res == -1 || res == 0)
		return 0;
	pDest[res] = '\0';
	char const* const pEnd = strstr(pDest, "\r\n\r\n");
	if (pEnd == NULL)
		return 0;
	const int PacketSize = pEnd - pDest + 4;
	recv(Socket, pDest, PacketSize, 0);
	pDest[PacketSize] = '\0';
	return PacketSize;
}

//-----------------------------------------------------------------------------

#define KEY_LEN 24
#define WEBSOCKET_UUID_LEN 36
#define WEBSOCKET_UUID "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define HANDSHAKE_ACCEPT_MSG               \
		"HTTP/1.1 101 Switching Protocols\r\n" \
		"Upgrade: websocket\r\n"               \
		"Connection: Upgrade\r\n"              \
		"Sec-WebSocket-Accept: "

//-----------------------------------------------------------------------------

static int GetKey(const char* szRequest, char* Key) {
	const char* Pos = strstr(szRequest, "Sec-WebSocket-Key");
	if (Pos == NULL)
		return 0;
	while (*Pos != '\0' && *Pos != '\n' && *Pos != ':')
		++Pos;
	if (*Pos != ':')
		return 0;
	++Pos;
	if (*Pos == ' ')
		++Pos;
	memcpy(Key, Pos, KEY_LEN);
	return 1;
}

//-----------------------------------------------------------------------------

static int ComputeResponseKey(const char* Key, char *ResponseKey) {
	strncpy(ResponseKey, Key, KEY_LEN);
	strncpy(ResponseKey + KEY_LEN, WEBSOCKET_UUID, WEBSOCKET_UUID_LEN);
	ResponseKey[KEY_LEN + WEBSOCKET_UUID_LEN] = '\0';
	return 1;
}

//-----------------------------------------------------------------------------

static int ComputeSha1(const char* String, int Len, char* Hash) {
	SHA1Context Context;
	SHA1Reset(&Context);
	SHA1Input(&Context, (const unsigned char*)String, Len);
	SHA1Result(&Context, (uint8_t*)Hash);
	return 1;
}

//-----------------------------------------------------------------------------

static int GetHandshakeResponse(const char* szRequest, char* szResponse) {
	char Key[KEY_LEN];
	if (!GetKey(szRequest, Key))
		return 0;
	char ResponseKey[KEY_LEN + WEBSOCKET_UUID_LEN + 1];
	if (!ComputeResponseKey(Key, ResponseKey))
		return 0;
	char Hash[SHA1HashSize];
	if (!ComputeSha1(ResponseKey, sizeof(ResponseKey) - 1, Hash))
		return 0;
	unsigned char* EncodedKey = base64_encode((unsigned char*)Hash, sizeof(Hash), NULL);
	EncodedKey[strlen((char*)EncodedKey) - 1] = '\0';
	strcpy(szResponse, HANDSHAKE_ACCEPT_MSG);
	strcat(szResponse, (const char*)EncodedKey);
	strcat(szResponse, "\r\nSec-WebSocket-Protocol: binary");
	strcat(szResponse, "\r\n\r\n");
	free(EncodedKey);
	return 1;
}

//-----------------------------------------------------------------------------

int DoHandshake(int ClientSocket, const char* szRequest) {
	char szResponse[2048];
	if (!GetHandshakeResponse(szRequest, szResponse))
		return 0;
	const int res = send(ClientSocket, szResponse, strlen(szResponse), 0);
	if (res == -1 || res == 0)
		return 0;
	return 1;
}

//-----------------------------------------------------------------------------
