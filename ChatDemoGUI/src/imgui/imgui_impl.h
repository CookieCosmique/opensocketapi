#pragma once

struct GLFWwindow;

bool /*bSuccess*/ ImGuiInit(GLFWwindow* window, bool bInstallCallbacks);
void ImGuiShutdown();

void ImGuiNewFrame();
void ImGuiDraw();
