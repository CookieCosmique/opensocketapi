#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
# include <vadefs.h>
#else
# include <stdint.h>

# define INVALID_SOCKET -1
# define SOCKET_ERROR -1

#endif

typedef int OsaBool;
typedef unsigned char OsaByte;
typedef uintptr_t OsaUintptr;

//-----------------------------------------------------------------------------

#define OSA_TRUE 1
#define OSA_FALSE 0

#define OUT

#define OSA_INVALID_CONTEXT ((OsaUintptr)~0)
#define OSA_NULL_CONTEXT ((OsaUintptr)0)
#define OSA_NULL_CLIENT ((OsaUintptr)0)

enum {
	OSA_MAX_ADDRESS_LEN = 16,
	OSA_MAX_PORT_LEN = 8,
	OSA_MAX_PACKET_SIZE = 256
};

//-----------------------------------------------------------------------------

typedef enum OsaError {
	OsaNoError = 0,
	OsaErrorInvalidParameter,
	OsaErrorSocket,
	OsaErrorClientNotConnected,
	OsaErrorDisconnected,
	OsaErrorPacketSize,
	OsaErrorInvalidPacketHeader
} OsaError;

typedef void (*OsaErrorCallback)(const char* FunctionName, const char* FileName, int Line, const char* Condition, const char* ErrorStr);

//-----------------------------------------------------------------------------
//------------------------------- SERVER API ----------------------------------
//-----------------------------------------------------------------------------

//-------- Types

typedef struct OsaServerLaunchParameters {
	char Address[OSA_MAX_ADDRESS_LEN];
	int Port;
	void* UserDatas;
	OsaErrorCallback ServerErrorCallback;
} OsaServerLaunchParameters;

typedef OsaUintptr OsaServerContext;
typedef OsaUintptr OsaServerClient;

//-------- Context init/shutdown

OsaServerContext OsaServer_Launch(const OsaServerLaunchParameters* ServerLaunchParameters);
OsaError OsaServer_Shutdown(OsaServerContext ServerContext);

//-------- Callbacks

typedef OsaBool /*AcceptConnection*/ (*OsaServerClientConnectCallback)(OsaServerContext ServerContext, void* ContextDatas, OsaServerClient Client, OUT void** ClientDatas);
typedef void (*OsaServerClientDisconnectCallback)(OsaServerContext ServerContext, void* ContextDatas, OsaServerClient Client, void* ClientDatas);
typedef void (*OsaServerPacketReceivedCallback)(OsaServerContext ServerContext, void* ContextDatas, OsaServerClient Client, void* ClientDatas, const OsaByte* Packet, unsigned int PacketSize);

OsaError OsaServer_SetClientConnectCallback(OsaServerContext ServerContext, OsaServerClientConnectCallback Callback);
OsaError OsaServer_SetClientDisconnectCallback(OsaServerContext ServerContext, OsaServerClientDisconnectCallback Callback);
OsaError OsaServer_SetPacketReceivedCallback(OsaServerContext ServerContext, OsaServerPacketReceivedCallback Callback);

//-------- Polling

OsaError OsaServer_PollEvents(OsaServerContext ServerContext);

//-------- Actions

OsaError OsaServer_SendPacketToClient(OsaServerContext ServerContext, OsaServerClient Client, const OsaByte* Packet, unsigned int PacketSize);
OsaError OsaServer_DisconnectClient(OsaServerContext ServerContext, OsaServerClient Client);

//-------- Reading

OsaBool OsaServer_CanAcceptConnections(OsaServerContext ServerContext);
OsaBool OsaServer_IsClientConnected(OsaServerContext ServerContext, OsaServerClient Client);

OsaError OsaServer_GetAddressStr(OsaServerContext ServerContext, OUT char Address[OSA_MAX_ADDRESS_LEN]);
OsaError OsaServer_GetPortStr(OsaServerContext ServerContext, OUT char Port[OSA_MAX_PORT_LEN]);

OsaError OsaServer_GetClientAddressStr(OsaServerContext ServerContext, OsaServerClient Client, OUT char Address[OSA_MAX_ADDRESS_LEN]);
OsaError OsaServer_GetClientPortStr(OsaServerContext ServerContext, OsaServerClient Client, OUT char Port[OSA_MAX_PORT_LEN]);

//-----------------------------------------------------------------------------
//------------------------------- CLIENT API ----------------------------------
//-----------------------------------------------------------------------------

//-------- Types

typedef struct OsaClientConnectParameters {
	char Address[OSA_MAX_ADDRESS_LEN];
	int Port;
	void* UserDatas;
	OsaErrorCallback ClientErrorCallback;
} OsaClientConnectParameters;

typedef OsaUintptr OsaClientContext;

//-------- Context init/shutdown

OsaClientContext OsaClient_Connect(const OsaClientConnectParameters* ClientConnectParameters);
OsaError OsaClient_Disconnect(OsaClientContext ClientContext);

//-------- Callbacks

typedef void (*OsaClientConnectCallback)(OsaClientContext ClientContext, void* ClientContextDatas);
typedef void (*OsaClientPacketReceivedCallback)(OsaClientContext ClientContext, void* ClientContextDatas, const OsaByte* Packet, unsigned int PacketSize);
typedef void (*OsaClientDisconnectCallback)(OsaClientContext ClientContext, void* ClientContextDatas);

OsaError OsaClient_SetConnectCallback(OsaClientContext ClientContext, OsaClientConnectCallback Callback);
OsaError OsaClient_SetPacketReceivedCallback(OsaClientContext ClientContext, OsaClientPacketReceivedCallback Callback);
OsaError OsaClient_SetDisconnectCallback(OsaClientContext ClientContext, OsaClientDisconnectCallback Callback);

//-------- Polling

OsaError OsaClient_PollEvents(OsaClientContext ClientContext);

//-------- Actions

OsaError OsaClient_SendPacketToServer(OsaClientContext ClientContext, const OsaByte* Packet, unsigned int PacketSize);

//-------- Reading

OsaBool OsaClient_IsConnected(OsaClientContext ClientContext);

OsaError OsaClient_GetServerAddressStr(OsaClientContext ClientContext, OUT char Address[OSA_MAX_ADDRESS_LEN]);
OsaError OsaClient_GetServerPortStr(OsaClientContext ClientContext, OUT char Port[OSA_MAX_PORT_LEN]);

#ifdef __cplusplus
}
#endif
