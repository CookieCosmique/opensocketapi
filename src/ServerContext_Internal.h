#pragma once

#include "OpenSocketAPI.h"
#include "Platform.h"

enum {
	OSA_MAX_CLIENTS_COUNT = 256
};

typedef enum OsaClientType {
	OsaClientTypeUnknown = 0,
	OsaClientTypeSocket,
	OsaClientTypeWebSocket
} OsaClientType;

typedef struct OsaServerClient_t {
	SOCKET _Socket;
	OsaBool _Connected;
	char _IpAddress[OSA_MAX_ADDRESS_LEN];
	char _Port[OSA_MAX_PORT_LEN];
	void* _UserDatas;
	enum OsaClientType _Type;
	OsaBool _HasSentConnectionPacket;
} OsaServerClient_t;

typedef struct OsaServerContext_t {
	// Do not change members (see Errors.h)
	SOCKET _Socket;

	char _IpAddress[OSA_MAX_ADDRESS_LEN];
	char _Port[OSA_MAX_PORT_LEN];

	OsaErrorCallback _ErrorCallback;
	//-------------------------------------
	OsaServerClientConnectCallback _ServerClientConnectCallback;
	OsaServerClientDisconnectCallback _ServerClientDisconnectCallback;
	OsaServerPacketReceivedCallback _ServerPacketReceivedCallback;

	OsaServerClient_t Clients[OSA_MAX_CLIENTS_COUNT];

	void* _UserDatas;
} OsaServerContext_t;

//-------- Context init/shutdown

OsaServerContext OsaServer_Launch_Internal(const OsaServerLaunchParameters* ServerLaunchParameters);
OsaError OsaServer_Shutdown_Internal(OsaServerContext_t* Context);

//-------- Callback

OsaError OsaServer_SetClientConnectCallback_Internal(OsaServerContext_t* Context, OsaServerClientConnectCallback Callback);
OsaError OsaServer_SetClientDisconnectCallback_Internal(OsaServerContext_t* Context, OsaServerClientDisconnectCallback Callback);
OsaError OsaServer_SetPacketReceivedCallback_Internal(OsaServerContext_t* Context, OsaServerPacketReceivedCallback Callback);

//-------- Polling

OsaError OsaServer_PollEvents_Internal(OsaServerContext_t* Context);

//-------- Actions

OsaError OsaServer_SendPacketToClient_Internal(OsaServerContext_t* Context, OsaServerClient_t* Client, const OsaByte* Packet, unsigned int PacketSize);
OsaError OsaServer_DisconnectClient_Internal(OsaServerContext_t* Context, OsaServerClient_t* Client);

//-------- Reading

OsaBool OsaServer_CanAcceptConnections_Internal(OsaServerContext_t* Context);
OsaBool OsaServer_IsClientConnected_Internal(OsaServerClient_t* Client);

OsaError OsaServer_GetAddressStr_Internal(OsaServerContext_t* Context, OUT char Address[OSA_MAX_ADDRESS_LEN]);
OsaError OsaServer_GetPortStr_Internal(OsaServerContext_t* Context, OUT char Port[OSA_MAX_PORT_LEN]);

OsaError OsaServer_GetClientAddressStr_Internal(OsaServerClient_t* Client, OUT char Address[OSA_MAX_ADDRESS_LEN]);
OsaError OsaServer_GetClientPortStr_Internal(OsaServerClient_t* Client, OUT char Port[OSA_MAX_PORT_LEN]);
