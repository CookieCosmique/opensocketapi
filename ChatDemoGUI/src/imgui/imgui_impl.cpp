#include "imgui_impl.h"
//----
#include <imgui.h>
//----
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
//-----------------------------------------------------------------------------
bool /*bSuccess*/ ImGuiInit(GLFWwindow* window, bool bInstallCallbacks) {
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	if (!ImGui_ImplGlfw_InitForOpenGL(window, bInstallCallbacks))
		return false;
	if (!ImGui_ImplOpenGL3_Init())
		return false;
	return true;
}
//-----------------------------------------------------------------------------
void ImGuiShutdown() {
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}
//-----------------------------------------------------------------------------
void ImGuiNewFrame() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}
//-----------------------------------------------------------------------------
void ImGuiDraw() {
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
//-----------------------------------------------------------------------------
