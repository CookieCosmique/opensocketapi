#pragma once

#ifdef __EMSCRIPTEN__
# define GLFW_INCLUDE_ES3
# include <GLFW/glfw3.h>
#else
# if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#  define GLFW_EXPOSE_NATIVE_WIN32
#  include <windows.h>
# endif
# include <GLFW/glfw3.h>
# include <GLFW/glfw3native.h>
#endif
