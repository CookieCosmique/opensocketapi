#pragma once

#include "Platform/PlatformDefines.h"

#ifdef __APPLE__
# define GLFW_INCLUDE_GLCOREARB
#elif defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
# define GLFW_EXPOSE_NATIVE_WIN32
# include <windows.h>
# include <windef.h>
#elif defined(__linux__)
# include <stdlib.h>
#elif defined(__EMSCRIPTEN__)
# define GL_GLEXT_PROTOTYPES 1
# include <GLES3/gl3.h>
# include <GLES3/gl2ext.h>
# include <emscripten.h>
# include <GL/gl.h>
# include <GL/glext.h>
#endif

#if USE_GLEW()
# define GLEW_STATIC
# include <GL/glew.h>
#endif
