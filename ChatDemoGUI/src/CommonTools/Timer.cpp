#include "Timer.h"
//----
#include "Platform/GetTime.h"
//----
#include "CommonTools/Assert.h"
//-----------------------------------------------------------------------------
uint64 /*DiffInUsec*/ TIMER::Reset() {
	const uint64 PreviousTimeInMicroSeconds = TimeInMicroSeconds;
	TimeInMicroSeconds = GetCurrentTimeInMicroSeconds();
	AssertWarning(TimeInMicroSeconds >= PreviousTimeInMicroSeconds);
	return TimeInMicroSeconds - PreviousTimeInMicroSeconds;
}
//-----------------------------------------------------------------------------
