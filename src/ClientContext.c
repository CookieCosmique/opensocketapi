#include "OpenSocketAPI.h"
#include "ClientContext_Internal.h"
#include "Errors.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#define CheckContextValue(_RETURN_VALUE_) ReturnError(Context == OSA_INVALID_CONTEXT || Context == OSA_NULL_CONTEXT, _RETURN_VALUE_)
#define CheckBufferValue(_BUFFER_, _RETURN_VALUE_) ReturnError(_BUFFER_ == NULL, _RETURN_VALUE_)

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

static inline OsaClientContext_t* ToClientContext(OsaClientContext Context) {
	return (OsaClientContext_t*)Context;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

OsaClientContext OsaClient_Connect(const OsaClientConnectParameters* ClientConnectParameters) {
	if (ClientConnectParameters == NULL)
		return OSA_INVALID_CONTEXT;
	return OsaClient_Connect_Internal(ClientConnectParameters);
}

//-----------------------------------------------------------------------------

OsaError OsaClient_Disconnect(OsaClientContext Context) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaClient_Disconnect_Internal(ToClientContext(Context));
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SetConnectCallback(OsaClientContext Context, OsaClientConnectCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaClient_SetConnectCallback_Internal(ToClientContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SetPacketReceivedCallback(OsaClientContext Context, OsaClientPacketReceivedCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaClient_SetPacketReceivedCallback_Internal(ToClientContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SetDisconnectCallback(OsaClientContext Context, OsaClientDisconnectCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaClient_SetDisconnectCallback_Internal(ToClientContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaBool OsaClient_IsConnected(OsaClientContext Context) {
	CheckContextValue(OSA_FALSE);
	return OsaClient_IsConnected_Internal(ToClientContext(Context));
}

//-----------------------------------------------------------------------------

OsaError OsaClient_PollEvents(OsaClientContext Context) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaClient_PollEvents_Internal(ToClientContext(Context));
}

//-----------------------------------------------------------------------------

OsaError OsaClient_SendPacketToServer(OsaClientContext Context, const OsaByte* Packet, unsigned int PacketSize) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckBufferValue(Packet, OsaErrorInvalidParameter);
	ReturnError(PacketSize == 0, OsaErrorInvalidParameter);
	return OsaClient_SendPacketToServer_Internal(ToClientContext(Context), Packet, PacketSize);
}

//-----------------------------------------------------------------------------

OsaError OsaClient_GetServerAddressStr(OsaClientContext Context, OUT char Address[OSA_MAX_ADDRESS_LEN]) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckBufferValue(Address, OsaErrorInvalidParameter);
	return OsaClient_GetServerAddressStr_Internal(ToClientContext(Context), OUT Address);
}

//-----------------------------------------------------------------------------

OsaError OsaClient_GetServerPortStr(OsaClientContext Context, OUT char Port[OSA_MAX_PORT_LEN]) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckBufferValue(Port, OsaErrorInvalidParameter);
	return OsaClient_GetServerPortStr_Internal(ToClientContext(Context), OUT Port);
}

//-----------------------------------------------------------------------------
