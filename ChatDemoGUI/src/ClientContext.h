#pragma once

#include "OpenSocketAPI.h"

#include "CommonTools/StaticArray.h"

typedef char MESSAGE[256];

struct CLIENT_CONTEXT {
	OsaClientContext Context = OSA_INVALID_CONTEXT;
	STATIC_ARRAY<MESSAGE, 256> Messages;
	bool bDisconnected;
	//----------
	bool /*bSuccess*/ Connect();
	void Disconnect();
	void Update();
	void DrawUI();
};
