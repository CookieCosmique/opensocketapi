#pragma once
//----
#include "Platform/Types.h"
//-----------------------------------------------------------------------------
struct TIMER {
	uint64 TimeInMicroSeconds;
	//----------
	TIMER(): TimeInMicroSeconds(0) { Reset(); }
	uint64 /*DiffInMicroSeconds*/ Reset();
};
//-----------------------------------------------------------------------------
