#pragma once

#define INVALID_PACKET_HEADER (-1)
#define PACKET_MAGIC_VALUE 0x3579E73C
#define CONNECTION_PACKET 0x66AA70B6
#define DISCONNECTION_PACKET 0x8EFA78A5

typedef struct PacketHeader {
	unsigned int MagicValue;
	unsigned int PacketSize;
} PacketHeader;

static inline void InitHeader(OUT PacketHeader* Header) {
	Header->MagicValue = PACKET_MAGIC_VALUE;
}

static inline int IsHeaderValid(PacketHeader Header) {
	return Header.MagicValue == PACKET_MAGIC_VALUE;
}
