#pragma once

#if defined(NDEBUG)
# define DEBUG() 0
#elif defined(_DEBUG) || defined(DEBUG)
# define DEBUG() 1
#else
# error Failed to detect debug/release configuration
# define DEBUG() 0
#endif

#define OUT
#define IN_OUT
