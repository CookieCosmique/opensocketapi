#include "Window.h"
//----
#include "Platform/PlatformDefines.h"
//----
#include "CommonTools/Logger.h"
#include "CommonTools/Assert.h"
//----
#if USE_GLEW()
# include "OpenGL.h"
#endif
//----
#include "glfw.h"
//-----------------------------------------------------------------------------
namespace {
	void OpenGLErrorCallback(int ID, const char* szDescription) {
		AssertError(false);
		LOGGER::Log(LOGGER::LOG_ERROR, "OpenGL Error %d: %s", ID, szDescription);
	}
	//-----------------------------------------------------------------------------
#if USE_GLEW()
	bool /*bSuccess*/ InitGlew() {
		glewExperimental = GL_TRUE;
		const GLenum Error = glewInit();
		AssertError(Error == GLEW_OK);
		if (Error != GLEW_OK)
			LOGGER::Log(LOGGER::LOG_ERROR, "Error while initializing glew: %s", glewGetErrorString(Error));
		return Error == GLEW_OK;
	}
#endif
} //-- namespace
//-----------------------------------------------------------------------------
bool /*bSuccess*/ WINDOW::OpenWindow(char const* const szTitle, int32 Width, int32 Height) {
	glfwSetErrorCallback(&OpenGLErrorCallback);
	if (glfwInit() == GLFW_FALSE) {
		AssertError(false);
		LOGGER::Log(LOGGER::LOG_ERROR, "glfwInit() error");
		return false;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_FOCUSED, GL_TRUE);
	glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
	glfwWindowHint(GLFW_DECORATED, GL_TRUE);

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	pWindow = glfwCreateWindow(Width, Height, szTitle, nullptr, nullptr);

	if (pWindow == nullptr) {
		AssertError(false);
		LOGGER::Log(LOGGER::LOG_ERROR, "Failed to create window.");
		return false;
	}

	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(pWindow, (mode->width - Width) / 2, (mode->height - Height) / 2);

	glfwMakeContextCurrent(pWindow);

#ifndef __EMSCRIPTEN__
	glfwSwapInterval(0);
#endif

#if USE_GLEW()
	if (!InitGlew()) {
		CloseWindow();
		return false;
	}
#endif

	AssertFatal(glGenVertexArrays != NULL);
	return true;
}
//-----------------------------------------------------------------------------
void WINDOW::CloseWindow() {
	AssertFatal(pWindow != nullptr);
	glfwDestroyWindow(pWindow);
	glfwTerminate();
}
//-----------------------------------------------------------------------------
void WINDOW::Draw() const {
	AssertFatal(pWindow != nullptr);
	glfwSwapBuffers(pWindow);
	glfwPollEvents();
}
//-----------------------------------------------------------------------------
bool WINDOW::IsCloseRequested() const {
	AssertFatal(pWindow != nullptr);
	return glfwWindowShouldClose(pWindow);
}
//-----------------------------------------------------------------------------
