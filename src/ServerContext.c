#include "OpenSocketAPI.h"
#include "ServerContext_Internal.h"
#include "Errors.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#define CheckContextValue(_RETURN_VALUE_) ReturnError(Context == OSA_INVALID_CONTEXT || Context == OSA_NULL_CONTEXT, _RETURN_VALUE_)
#define CheckClientValue(_RETURN_VALUE_) ReturnError(Client == OSA_NULL_CLIENT, _RETURN_VALUE_)
#define CheckBufferValue(_BUFFER_, _RETURN_VALUE_) ReturnError(_BUFFER_ == NULL, _RETURN_VALUE_)

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

static inline OsaServerContext_t* ToServerContext(OsaServerContext Context) {
	return (OsaServerContext_t*)Context;
}

//-----------------------------------------------------------------------------

static inline OsaServerClient_t* ToServerClient(OsaServerClient ServerClient) {
	return (OsaServerClient_t*)ServerClient;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

OsaServerContext OsaServer_Launch(const OsaServerLaunchParameters* ServerLaunchParameters) {
	if (ServerLaunchParameters == NULL)
		return OSA_INVALID_CONTEXT;
	return OsaServer_Launch_Internal(ServerLaunchParameters);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_Shutdown(OsaServerContext Context) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaServer_Shutdown_Internal(ToServerContext(Context));
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SetClientConnectCallback(OsaServerContext Context, OsaServerClientConnectCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaServer_SetClientConnectCallback_Internal(ToServerContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SetClientDisconnectCallback(OsaServerContext Context, OsaServerClientDisconnectCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaServer_SetClientDisconnectCallback_Internal(ToServerContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SetPacketReceivedCallback(OsaServerContext Context, OsaServerPacketReceivedCallback Callback) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaServer_SetPacketReceivedCallback_Internal(ToServerContext(Context), Callback);
}

//-----------------------------------------------------------------------------

OsaBool OsaServer_CanAcceptConnections(OsaServerContext Context) {
	CheckContextValue(OSA_FALSE);
	return OsaServer_CanAcceptConnections_Internal(ToServerContext(Context));
}

//-----------------------------------------------------------------------------

OsaBool OsaServer_IsClientConnected(OsaServerContext Context, OsaServerClient Client) {
	CheckClientValue(OSA_FALSE);
	return OsaServer_IsClientConnected_Internal(ToServerClient(Client));
}

//-----------------------------------------------------------------------------

OsaError OsaServer_PollEvents(OsaServerContext Context) {
	CheckContextValue(OsaErrorInvalidParameter);
	return OsaServer_PollEvents_Internal(ToServerContext(Context));
}

//-----------------------------------------------------------------------------

OsaError OsaServer_SendPacketToClient(OsaServerContext Context, OsaServerClient Client, const OsaByte* Packet, unsigned int PacketSize) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckClientValue(OsaErrorInvalidParameter);
	CheckBufferValue(Packet, OsaErrorInvalidParameter);
	ReturnError(PacketSize == 0, OsaErrorInvalidParameter);
	return OsaServer_SendPacketToClient_Internal(ToServerContext(Context), ToServerClient(Client), Packet, PacketSize);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_DisconnectClient(OsaServerContext Context, OsaServerClient Client) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckClientValue(OsaErrorInvalidParameter);
	return OsaServer_DisconnectClient_Internal(ToServerContext(Context), ToServerClient(Client));
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetAddressStr(OsaServerContext Context, OUT char Address[OSA_MAX_ADDRESS_LEN]) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckBufferValue(Address, OsaErrorInvalidParameter);
	return OsaServer_GetAddressStr_Internal(ToServerContext(Context), OUT Address);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetPortStr(OsaServerContext Context, OUT char Port[OSA_MAX_PORT_LEN]) {
	CheckContextValue(OsaErrorInvalidParameter);
	CheckBufferValue(Port, OsaErrorInvalidParameter);
	return OsaServer_GetPortStr_Internal(ToServerContext(Context), OUT Port);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetClientAddressStr(OsaServerContext Context, OsaServerClient Client, OUT char Address[OSA_MAX_ADDRESS_LEN]) {
	CheckClientValue(OsaErrorInvalidParameter);
	CheckBufferValue(Address, OsaErrorInvalidParameter);
	return OsaServer_GetClientAddressStr_Internal(ToServerClient(Client), OUT Address);
}

//-----------------------------------------------------------------------------

OsaError OsaServer_GetClientPortStr(OsaServerContext Context, OsaServerClient Client, OUT char Port[OSA_MAX_PORT_LEN]) {
	CheckClientValue(OsaErrorInvalidParameter);
	CheckBufferValue(Port, OsaErrorInvalidParameter);
	return OsaServer_GetClientPortStr_Internal(ToServerClient(Client), OUT Port);
}

//-----------------------------------------------------------------------------
