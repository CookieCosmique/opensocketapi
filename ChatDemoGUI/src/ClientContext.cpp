#include "ClientContext.h"
//----
#include "CommonTools/Assert.h"
#include "CommonTools/Logger.h"
//----
#include <imgui.h>
//----
#include <stdio.h>
//-----------------------------------------------------------------------------
namespace {
	void SocketErrorCallback(const char* FunctionName, const char* FileName, int Line, const char* Condition, const char* ErrorStr) {
		AssertError(false);
		LOGGER::Log(LOGGER::LOG_ERROR, "%s (%s:%d) (%s - %s)", ErrorStr, FileName, Line, FunctionName, Condition);
	}
	//-----------------------------------------------------------------------------
	void PacketReceivedCallback(OsaClientContext /*Context*/, void* ContextDatas, const OsaByte* Packet, unsigned int PacketSize) {
		CLIENT_CONTEXT& m = *(CLIENT_CONTEXT*)ContextDatas;
		MESSAGE& NewMessage = m.Messages.Add();
		memcpy(NewMessage, Packet, PacketSize);
	}
	//-----------------------------------------------------------------------------
	void DisconnectionCallback(OsaClientContext /*Context*/, void* ContextDatas) {
		CLIENT_CONTEXT& m = *(CLIENT_CONTEXT*)ContextDatas;
		m.bDisconnected = true;
	}
	//-----------------------------------------------------------------------------
	OsaClientConnectParameters FillContextParameters(CLIENT_CONTEXT& m) {
		const char* IpAddress = "5.51.81.153";
		int Port = 200;
		LOGGER::Log(LOGGER::LOG_DEBUG, "Connecting to %s:%u", IpAddress, Port);

		OsaClientConnectParameters ContextParameters;
		memcpy(ContextParameters.Address, IpAddress, strlen(IpAddress) + 1);
		ContextParameters.Port = (int)Port;
		ContextParameters.UserDatas = &m;
		ContextParameters.ClientErrorCallback = SocketErrorCallback;
		return ContextParameters;
	}
	//-----------------------------------------------------------------------------
	void RegisterClientCallbacks(OsaClientContext Context) {
		OsaClient_SetPacketReceivedCallback(Context, PacketReceivedCallback);
		OsaClient_SetDisconnectCallback(Context, DisconnectionCallback);
	}
	//-----------------------------------------------------------------------------
	void LogServerAddress(OsaClientContext Context) {
		if (!LOGGER::IsEnabled())
			return;
		char ServerAddress[OSA_MAX_ADDRESS_LEN];
		char ServerPort[OSA_MAX_PORT_LEN];
		OsaClient_GetServerAddressStr(Context, ServerAddress);
		OsaClient_GetServerPortStr(Context, ServerPort);
		LOGGER::Log(LOGGER::LOG_INFO, "Client connected on server %s:%s", ServerAddress, ServerPort);
	}
} //-- namespace
//-----------------------------------------------------------------------------
bool /*bSuccess*/ CLIENT_CONTEXT::Connect() {
	bDisconnected = false;
	const OsaClientConnectParameters ContextParameters = FillContextParameters(*this);
	Context = OsaClient_Connect(&ContextParameters);
	AssertErrorLogAndReturnValue(Context != OSA_INVALID_CONTEXT, false, "Failed to connect to server.");
	RegisterClientCallbacks(Context);
	LogServerAddress(Context);
	return true;
}
//-----------------------------------------------------------------------------
void CLIENT_CONTEXT::Disconnect() {
	CLIENT_CONTEXT& m = *this;
	MESSAGE& Message = m.Messages.Add();
	snprintf(Message, sizeof(MESSAGE), "-- Disconnected --");
	bDisconnected = true;
	AssertErrorLogAndReturn(Context != OSA_INVALID_CONTEXT, "Cannot disconnect client: Invalid context.");
	OsaClient_Disconnect(Context);
}
//-----------------------------------------------------------------------------
void CLIENT_CONTEXT::Update() {
	OsaClient_PollEvents(Context);
}
//-----------------------------------------------------------------------------
void CLIENT_CONTEXT::DrawUI() {
	CLIENT_CONTEXT& m = *this;

	char ServerAddress[OSA_MAX_ADDRESS_LEN];
	char ServerPort[OSA_MAX_PORT_LEN];
	OsaClient_GetServerAddressStr(Context, ServerAddress);
	OsaClient_GetServerPortStr(Context, ServerPort);
	ImGui::Text("Client connected to %s:%s", ServerAddress, ServerPort);

	const int bIsConnected = OsaClient_IsConnected(Context);
	ImGui::Text("Status: %s", bIsConnected ? "CONNECTED" : "PENDING");

	ImGui::PushStyleColor(ImGuiCol_ChildBg, 0xFF000000);

	if (ImGui::BeginChild("ChatMessages", ImVec2(ImGui::GetWindowWidth(), ImGui::GetContentRegionAvail().y))) {
		float CursorPosY = ImGui::GetWindowHeight() - ImGui::GetFontSize() - ImGui::GetStyle().ItemSpacing.y - ImGui::GetStyle().FramePadding.y * 2.f;
		ImGui::SetCursorPosY(CursorPosY);
		static MESSAGE InputMessage = {};
		static bool bFocusRequested = true;
		if (bFocusRequested) {
			ImGui::SetKeyboardFocusHere();
			bFocusRequested = false;
		}
		if (ImGui::InputText("", OUT InputMessage, sizeof(InputMessage), ImGuiInputTextFlags_EnterReturnsTrue)) {
			if (bIsConnected) {
				OsaClient_SendPacketToServer(m.Context, (OsaByte*)InputMessage, strlen(InputMessage) + 1);
			} else {
				MESSAGE& ErrorMessage = m.Messages.Add();
				snprintf(ErrorMessage, sizeof(ErrorMessage), "Cannot send message: client not connected.");
			}
			InputMessage[0] = '\0';
			bFocusRequested = true;
		}
		for (int iMessage = (int)m.Messages.Count - 1; iMessage >= 0; --iMessage) {
			const MESSAGE& Message = m.Messages[iMessage];
			CursorPosY -= ImGui::GetFontSize() + ImGui::GetStyle().ItemSpacing.y;
			ImGui::SetCursorPosY(CursorPosY);
			ImGui::Text("%s", Message);
		}
	}
	ImGui::EndChild();

	ImGui::PopStyleColor();
}
//-----------------------------------------------------------------------------
