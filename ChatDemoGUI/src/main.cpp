#include "Rendering/OpenGL.h"
#include "Rendering/Window.h"
//----
#include "Platform/MainLoop.h"
#include "Platform/Break.h"
//----
#include "CommonTools/Assert.h"
#include "CommonTools/Logger.h"
//----
#include "imgui/imgui_impl.h"
//----
#include "ServerContext.h"
#include "ClientContext.h"
//----
#include <imgui.h>
//----
#include <stdlib.h>
//-----------------------------------------------------------------------------
enum MODE {
	MODE_LOBBY,
	MODE_SERVER,
	MODE_CLIENT
};
//-----------------------------------------------------------------------------
struct CHAT_CONTEXT {
	WINDOW Window;
	MODE Mode;
	SERVER_CONTEXT Server;
	CLIENT_CONTEXT Client;
	//----------
	bool /*bSuccess*/ Init();
	void Shutdown();
	void Draw();
	void Launch();
};
//-----------------------------------------------------------------------------
namespace {
	//-----------------------------------------------------------------------------
	bool BeginGlobalWindow(char const* const szTitle) {
		ImGui::SetNextWindowPos(ImVec2(-5.f, 0.f), ImGuiCond_Always);
		ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x + 10.f, ImGui::GetIO().DisplaySize.y + 10.f), ImGuiCond_Always);
		bool pOpen = true;
		bool bMenuBar = false;
		return ImGui::Begin(szTitle, &pOpen, (bMenuBar ? ImGuiWindowFlags_MenuBar : 0) | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoBringToFrontOnFocus);
	}
	//-----------------------------------------------------------------------------
	void Update(CHAT_CONTEXT& m) {
		switch (m.Mode) {
		case MODE_LOBBY:
			break;
		case MODE_CLIENT:
			m.Client.Update();
			if (m.Client.bDisconnected) {
				m.Client.Disconnect();
				m.Mode = MODE_LOBBY;
			}
			break;
		case MODE_SERVER:
			m.Server.Update();
			break;
		default:
			Break();
			break;
		}
	}
	//-----------------------------------------------------------------------------
	void LoopCallback(void* pUserData) {
		CHAT_CONTEXT& m = *(CHAT_CONTEXT*)pUserData;
		Update(m);
		m.Draw();
	}
	//-----------------------------------------------------------------------------
	bool CloseCallback(void* pUserData) {
		CHAT_CONTEXT& m = *(CHAT_CONTEXT*)pUserData;
		return m.Window.IsCloseRequested();
	}
	//-----------------------------------------------------------------------------
	void DrawUI(CHAT_CONTEXT& m) {
		switch (m.Mode) {
		case MODE_LOBBY:
			if (ImGui::Button("Client")) {
				if (m.Client.Connect()) {
					m.Mode = MODE_CLIENT;
				}
			}
#ifndef __EMSCRIPTEN__
			if (ImGui::Button("Server")) {
				if (m.Server.Open()) {
					m.Mode = MODE_SERVER;
				}
			}
#endif
			break;
		case MODE_CLIENT:
			if (ImGui::Button("Close")) {
				m.Client.Disconnect();
				m.Mode = MODE_LOBBY;
			} else {
				m.Client.DrawUI();
			}
			break;
		case MODE_SERVER:
			if (ImGui::Button("Close")) {
				m.Server.Close();
				m.Mode = MODE_LOBBY;
			} else {
				m.Server.DrawUI();
			}
			break;
		default:
			Break();
			break;
		}
	}
	//-----------------------------------------------------------------------------
} //-- namespace
//-----------------------------------------------------------------------------
bool /*bSuccess*/ CHAT_CONTEXT::Init() {
	if (!Window.OpenWindow("ChatDemoGUI", 1280, 720))
		return false;
	if (!ImGuiInit(Window.GetWindow(), true))
		return false;
	Mode = MODE_LOBBY;
	return true;
}
//-----------------------------------------------------------------------------
void CHAT_CONTEXT::Shutdown() {
	CHAT_CONTEXT& m = *this;
	switch (m.Mode) {
	case MODE_LOBBY:
		break;
	case MODE_CLIENT:
		m.Client.Disconnect();
		break;
	case MODE_SERVER:
		m.Server.Close();
		break;
	default:
		Break();
		break;
	}
	ImGuiShutdown();
	m.Window.CloseWindow();
}
//-----------------------------------------------------------------------------
void CHAT_CONTEXT::Draw() {
	ImGuiNewFrame();
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT);
	if (BeginGlobalWindow("Chat")) {
		DrawUI(*this);
	}
	ImGui::End();
	ImGuiDraw();
	Window.Draw();
}
//-----------------------------------------------------------------------------
void CHAT_CONTEXT::Launch() {
	MainLoop(LoopCallback, CloseCallback, this);
}
//-----------------------------------------------------------------------------
int main() {
	LOGGER::Enable();
	CHAT_CONTEXT Context;
	if (!Context.Init())
		return EXIT_FAILURE;
	Context.Launch();
	Context.Shutdown();
	return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------------
