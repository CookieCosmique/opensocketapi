#pragma once

int EncodeWebSocketFrame(unsigned char* msg, int msg_length, unsigned char* buffer, int buffer_size);
int DecodeWebSocketFrame(unsigned char* in_buffer, unsigned int in_length, unsigned char* out_buffer, unsigned int out_size, unsigned int* out_length, int* out_close_connection);
