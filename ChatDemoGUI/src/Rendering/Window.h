#pragma once

#include "Platform/Types.h"

struct GLFWwindow;

struct WINDOW {
private:
	GLFWwindow* pWindow = nullptr;

public:
	bool /*bSuccess*/ OpenWindow(char const* const szTitle, int32 Width, int32 Height);
	void CloseWindow();
	void Draw() const;
	bool IsCloseRequested() const;
	inline GLFWwindow* GetWindow() const { return pWindow; }
};
