#pragma once

#include "CommonTools/CommonDefines.h"

#ifndef Break
#if DEBUG()
	#if defined(_MSC_VER)
	# define Break __debugbreak
	#elif defined(__EMSCRIPTEN__)
	# define Break()
	#elif defined(__GNUC__)
	# define Break() asm("int3")
	#endif
#else
	# define Break()
#endif
#endif
